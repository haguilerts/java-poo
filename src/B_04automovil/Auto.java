package B_04automovil;

    public class Auto {
    // atributos de la clase
        String color;
        int anio;
        int velMaxima;
        String marca;

    // Constructores
    // Constructor vacio
        Auto(){  }
    // Constructor con 3 argumentos
        Auto(int anio, int velMaxima, String marca){
            this.anio=anio;
            this.velMaxima=velMaxima;
            this.marca=marca;
        }

    // Constructor con argumentos
        Auto(String color,int anio, int velMaxima,String marca){
            this.color=color;
            this.anio=anio;
            this.velMaxima=velMaxima;
            this.marca=marca;
        }

    // Metodos de la clase
    // Metodo de tocarbocina
        void tocarBocina() {
            System.out.println("Pi, Pi, PI, PI");
        }

        void acelerar() {
            System.out.println("Correte !!!!");
            System.out.println("Estoy acelerando, vuelo bajito");
        }

        void frenar(){
            System.out.println("Alto ahi!!");
            System.out.println("Estoy frenando el auto");
        }
        // Muestra los valores de los atributos
        void estado(){
            System.out.println("Color:"+color);
            System.out.println("Año de fabricacion:"+anio);
            System.out.println("Velocidad maxima:"+velMaxima);
            System.out.println("Marca:"+marca);
        }
}
