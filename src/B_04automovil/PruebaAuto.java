package B_04automovil;

public class PruebaAuto {
    public static void main(String[] args) {
        // Creo un objeto auto vacio
        Auto auto1=new Auto();
        System.out.println(auto1.anio);
        System.out.println(auto1.color);
        System.out.println(auto1.marca);
        System.out.println(auto1.velMaxima);
        // Creo un objeto pasando parametros
        System.out.println("---------------------");
        Auto auto2=new Auto("rojo",2018,200,"Renault");
        // Invocando un metodo
        auto2.tocarBocina();
        System.out.println("Voy a ver el estado del auto");
        System.out.println(auto2.anio);
        System.out.println(auto2.color);
        System.out.println(auto2.marca);
        System.out.println(auto2.velMaxima);
        System.out.println("------------------------");
        Auto auto3=new Auto(2018,250,"Chevrolet");
        auto3.acelerar(); // intancio el metodo 
        auto3.frenar();
        System.out.println(auto3.anio);// instancio el atributo
        System.out.println(auto3.color);
        System.out.println(auto3.marca);
        System.out.println(auto3.velMaxima);
        System.out.println(" Ver el estado de los autos invocando el metodo");
        auto1.estado();
        auto2.estado();
        auto3.estado();
    }
}
