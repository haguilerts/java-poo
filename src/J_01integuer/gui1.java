package J_01integuer;

import javax.swing.JOptionPane;

public class gui1 {

    public static void main(String args[]) {

        int numero1 = Integer.parseInt(JOptionPane.showInputDialog(
                null, "Introduzca un numero",
                "Sumador",
                JOptionPane.QUESTION_MESSAGE));

        int numero2 = Integer.parseInt(JOptionPane.showInputDialog(
                null, "Introduzca otro numero",
                "Sumador",
                JOptionPane.QUESTION_MESSAGE));

        int resultado = numero1 + numero2;

        JOptionPane.showMessageDialog(
                null, "Su suma es: " + resultado,
                "Sumador",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
