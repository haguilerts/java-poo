package B_03personas;

public  class Persona {
    // Atributos de la clase
    String nombre;
    String apellidoPaterno;
    String apellidoMaterno;
    static int edad=64;
    
    // Metodos de la clase
    // Son los metodos que usaran los objetos de esta clase
    public void desplegarNombre(){
        System.out.println("Nombre:"+nombre);
        System.out.println("Apellido paterno:"+apellidoPaterno);
        System.out.println("Apellido materno:"+apellidoMaterno);
    }
    
    
}
