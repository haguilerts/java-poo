package B_03personas;
import java.util.Calendar;
public class PersonasPruebas {
    public static void main(String[] args) {
       // Creo el objeto p1 del tipo persona
        Persona p1=new Persona();
       
        // LLamar al metodo de la clase Persona del objeto creado
        System.out.println("Mostrar los valores x defecto");
        p1.desplegarNombre();
        
        // Modificar los valores de los atributos 
        // de mi objeto p1
        p1.nombre="Juan";
        p1.apellidoPaterno="Esparza";
        p1.apellidoMaterno="Lara";
        
        //Volvemos ainvocar al metodo
        System.out.println("\nNuevos valores del objeto p1");
        p1.desplegarNombre();
        
        // Hacemos una variable p3
        System.out.println("Acceso a p3");
        Persona p3;
        p3=p1;
        p3.desplegarNombre();
        
        // accedo a la edad estatic 
        // Se puede importar un paquete entero o un componente del paquete. Por
        //ejemplo, si se desea importar las librerías para cálculos matemáticos de
        //Java.import java.math.*;
        int nuevaDelaClase=Persona.edad;
        System.out.println("La edad de la clase es:"+nuevaDelaClase);
        // otra forma 
        System.out.println("otra forma de edad: "+Math.sqrt(Persona.edad));
        
////////////////////////////////////////////////////////////////////////////////
        System.out.println("//////// calentario ///////");
////////////////////////////////////////////////////////////////////////////////       
        
        int edad, diaHoy, mesHoy, añoHoy;
            diaHoy = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            mesHoy = Calendar.getInstance().get(Calendar.MONTH) + 1;
            añoHoy = Calendar.getInstance().get(Calendar.YEAR);
            System.out.println("La fecha de hoy es " + diaHoy + "/" + mesHoy + "/" + añoHoy);
            
            System.out.println(p1.nombre.length());
    
            String [] Persona = new String []{"hola"};
            int [] Persona2 = new int []{25};
    }
        
}
