package D_01aulavirtual;

import java.util.Scanner;

public class Persona {
// Atributos
    private String nombre;
    private int edad;

// Atributo de clase
     private static String sexo;
    
    // Constructor vacio
    public Persona(){
        
    }
    // Constructor con 2 parametros
    public Persona(String nombre,int edad){
        this.nombre=nombre;
        this.edad=edad;
    }
    // cONSTRUCTOR CON 2 PARAMETROS
    public Persona(int edad, String nombre){
        this.nombre=nombre;
        this.edad=edad;
    }
    // Setters y getters
        public void setNombre(String nombre){
            this.nombre=nombre;
        }
        public String getNombre(){
            return this.nombre;
        }
        public void setEdad(int edad){
            this.edad=edad;
        }
        public int getEdad(){
            return this.edad;
        }
    // Metodos del atributo de la clase
    static public String getSexo(){
        return sexo;  }
    static public void setSexo(String sexo){
        Persona.sexo=sexo; }
    
    // Metodos de la clase
    public void obtener(){
        
        System.out.println("Ingrese su nombre:");
        Scanner teclado = new Scanner(System.in);
        nombre = teclado.nextLine();
        do {
            System.out.println("Ingresar edad:");
            edad =teclado.nextInt();
        } while (edad<=0);
    }
    public boolean esMayor(){
        boolean es=false;
        if (edad>18){
            es=true;
        }
        return es;
    }
    
    @Override
    public String toString(){
    // Muestre el estado del objeto
    return "Nombre:"+this.nombre + " Edad:"+this.edad;
            }    
}
