package D_01aulavirtual;

public class Vehiculo1 {
    // Atributos de la clase
    private String fabricante;
    private String matricula;
    private int anioFabricacion;
    private float velocidadMaxima;
    
    // Metodo constructor vacio
    public Vehiculo1 (){        
    }
    // Constructor con parametros
    public Vehiculo1(String fabricante, String matricula, int anioFabricacion,
            float velocidadMaxima) {
        this.fabricante = fabricante;
        this.matricula = matricula;
        this.anioFabricacion=anioFabricacion;
        this.velocidadMaxima = velocidadMaxima;
    }
    // Setters y getters
    public String getFabricante() {
        return fabricante;
    }
    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }
    public String getMatricula() {
        return matricula;
    }
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public int getAnioFabricacion() {
        return anioFabricacion;
    }
    public void setAnioFabricacion(int anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }
    public float getVelocidadMaxima() {
        return velocidadMaxima;
    }
    public void setVelocidadMaxima(float velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }
    
    // toString
    @Override
    public String toString(){
        return "Fabricante:"+fabricante+" Matricula:"+matricula+
                " Año Fabricacion:"+anioFabricacion+" Velocidad Maxima"+
                velocidadMaxima;
    }
    // Metodos
    public void arrancar(){
        System.out.println("Estoy arrancando el vehiculo");
    }
    public void apagar(){
        System.out.println("Estoy apagando el vehiculo");
    }
    public void acelerar(float velocidad){
        System.out.println("Estoy acelerando a:"+velocidad);
    }
    public void frenar(float velocidad){
        System.out.println("Estoy frenando a:"+velocidad);
    }
    
//    public void prueba(){
//        System.out.println("Estoy probando el vehiculo");
//    }
    
   
    
}
