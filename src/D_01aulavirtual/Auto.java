package D_01aulavirtual;

public class Auto extends Vehiculo1{
    // Atributos
    private String modelo;
    private String patente;
    // Constructores
    public Auto(String fabricante, String matricula, int anioFabricacion,
            float velocidadMaxima,String modelo, String patente) {
        super(fabricante,matricula,anioFabricacion,velocidadMaxima);
        this.modelo = modelo;
        this.patente = patente;
    }
    public Auto(){
//        super();
    }
    //Setter y getters
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }
    // Metodos
    public void tocarBocina(){
        System.out.println("PI PI PI");
    }
    // ´@Override´ se pone para indicar al compilador que tu intención es sobreescribir 
    //el método de la clase padre y si te equivocas (por ejemplo, al escribir el nombre 
    //del método) y no estás realmente sobreescribiendo, el compilador dará un error.
    @Override
    public void apagar(){
        System.out.println("Estoy apagando el auto");
    }
    @Override
    public String toString(){
        return super.toString()+" Modelo:"+modelo+" Patente:"+patente;
    }
}
