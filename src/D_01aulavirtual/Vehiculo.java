package D_01aulavirtual;

public class Vehiculo {
    // atributos
    private String marca;
    private String modelo;
    private String combustible;
    private int cilindrada;
    private float velocidad;
    private float velocidadMaxima=220; // Declaro y asigno
    
    // Metodos constructor
    // constructor vacio
    public Vehiculo(){}
    
    // constructor con 4 argumentos
    public Vehiculo(String marca,String modelo,String combustible,
            int cilindrada){
        this.marca=marca;
        this.modelo=modelo;
        this.combustible=combustible;
        this.cilindrada=cilindrada;
    }
    // Metodos setters y getters
    public void setVelocidad(float velocidad){
        this.velocidad=velocidad;
    }
    public float getVelocidad(){
        return this.velocidad;    }
    public void setMarca(String marca){
        this.marca=marca;    }
    public String getMarca(){
        return this.marca;    }
    public void setModelo(String modelo){
        this.modelo=modelo;    }
    public String getModelo(){
        return this.modelo;    }
    public void setCombustible(String combustible){
        this.combustible=combustible;    }
    public String getCombustible(){
        return this.combustible;    }
    public void setCilindrada(int cilindrada){
        this.cilindrada=cilindrada;    }
    public int getCilindrada(){
        return this.cilindrada;    }
    
    // toString
    @Override
    public String toString(){
        return "Marca:"+this.marca+" Modelo:"+this.modelo+
                " Combustible:"+ this.combustible+ " Cilindrada:"+
                this.cilindrada+" Velocidad:"+this.velocidad;
    }
    // metodos...
    public void arrancar(){
        System.out.println("Estoy arrancando el vehiculo");
    }
    
    public void apagar(){
        System.out.println("Estoy apagando el vehiculo");
    }
    
    public void acelerar(float velocidad){
        System.out.println("Estoy acelerando a "+velocidad+ "km/h");
        this.velocidad=this.velocidad+velocidad;
        if (this.velocidad>velocidadMaxima){
            this.velocidad=velocidadMaxima;
            System.out.println("Llegaste a la velocidad maxima");
        }
        System.out.println("Velocidad actual:"+this.velocidad);
    }
    
    public void frenar(float velocidad){
        System.out.println("Estoy frenando a "+velocidad+" km/h");
        this.velocidad-=velocidad;
        if (this.velocidad<0){
            this.velocidad=0;
            System.out.println("El auto esta detenido");
        }
        System.out.println("Velocidad actual:"+this.velocidad);
    }
    
}
