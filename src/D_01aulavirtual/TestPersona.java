package D_01aulavirtual;

public class TestPersona {

    public static void main(String[] args) {
        System.out.println("---Creo una alumno---");
        Persona alumno1=new Persona();  // creo un objeto del tipo persona
//        // Ingreso los datos mediante el metodo obtener
//        alumno1.obtener();// ingreso nombre y edad
//        // Mostrar los datos del alumno1 mediante los getters
        System.out.println("El alumno se llama:"+alumno1.getNombre());
        System.out.println("El alumno tiene:"+alumno1.getEdad()+" años");
        if (alumno1.esMayor()) System.out.println("El alumno es mayor");
//        System.out.println("--------------------------------------");
        Persona alumno2=new Persona("Jorge Olguin",62);
         System.out.println("El alumno se llama:"+alumno2.getNombre());
        System.out.println("El alumno tiene:"+alumno2.getEdad()+" años");
        if (alumno2.esMayor()) System.out.println("El alumno es mayor");
        // Sobreescribi el metodo toString
        // Que me da el estado del objeto
        System.out.println("alumno1: "+alumno1);// nul xq no tiene párametros
        System.out.println("alumno2: "+alumno2);
        System.out.println("alumno1toString: "+alumno1.toString());
        System.out.println("alumno2toString: "+alumno2.toString());
        System.out.println("--------------------------------------");
//        // Uso de null
        System.out.println("El alumno se llama:"+alumno1.getNombre());
        System.out.println("El alumno tiene:"+alumno1.getEdad()+" años");
//        alumno1=null;
//        System.out.println(alumno1);
        // Da error porque no apunta a nada el objeto
        // System.out.println("El alumno se llama:"+alumno1.getNombre());
        // System.out.println("El alumno tiene:"+alumno1.getEdad()+" años");
        // Doy valor al atributo de la clase
        // Para poder acceder lo hago a traves del nombre de a clase
        //Persona.sexo="Femenino"; 
        //no se puede xq sexo es privado solo se podra incertar o leer  atra ves de metodo set y get. 
          Persona.setSexo("femenino");
        System.out.println(Persona.getSexo());
////        Persona.sexo="Masculino";
////        System.out.println(Persona.sexo);
          Persona.setSexo("Masculino");
        System.out.println(Persona.getSexo());
//        Persona.setSexo("Femenino");
//        System.out.println(Persona.getSexo());
//        Persona.setSexo("Masculino");
//        System.out.println(Persona.getSexo());

// Dia 4 de setiembre
            System.out.println("Dia 4 de setiembre");
        Alumno alum1 = new Alumno();    // creo un objeto de la clase alumno
       
        System.out.println("Alum1:"+ alum1);// como existe toString no sale error.
        // caso contrario me tiraria este mensaje ´Alumno@52e922´ devido al no exixitir un toString
    //alum1.obtener();// ingresa nombre, edad, matricula
    //System.out.println("alum1.obtener();"+alum1.toString());
    
        System.out.println("----------------");
        // Creo el objeto alumno2
        Alumno alum2 = new Alumno();
        alum2.setNombre("Pancho Sa");
        alum2.setEdad(33);
        alum2.setMatricula("Mat1234-17");
        System.out.println("Alum2:");
        System.out.println(alum2);
        // dia 6 de setiembre
        // Creo un nuevo objeto
        Alumno alumno3 = new Alumno();
        alumno3.setEdad(45);
        alumno3.setNombre ("Ruben Suñe");
        alumno3.setMatricula("Len1234-15");
        System.out.println("Alumno3");// 
        System.out.println(alumno3);
        // Creo un nuevo objeto
        System.out.println("--------------------------");
        // Creo un nuevo objeto con los datos inicializados
        Alumno alumno4= new Alumno("Dario Felman",45,"Hist8745");
        System.out.println("Alumno4:");
        System.out.println(alumno4);
    }
}
