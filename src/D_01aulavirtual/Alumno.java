package D_01aulavirtual;

import java.util.Scanner;

public class Alumno extends Persona{
    // Atributos de la clase Alumno
    private String matricula;
    
   // Metodo constructor vacio
    public Alumno(){
    }
    // Metodo constructor con 3 parametros
    public Alumno(String nombre,int edad,String matricula){
        // super(nombre,edad);  // llamo al 2do constructor de Persona
        super(edad,nombre);     // llamo al ul 3er constructor de Persona
        this.matricula=matricula;
    }
    
    // Metodos getter y setter de Alumnos
    public void setMatricula(String matricula){
        this.matricula=matricula;
    }
    public String getMatricula(){
        return this.matricula;
    }
    
    // Metodo obtener
    @Override
    public void obtener(){
        // Accedo al metodo obtener de la superclase
        // con el operador super
        super.obtener();// ingresa nombre, edad
        System.out.println("Ingrese la matricula del alumno:");
        Scanner teclado=new Scanner(System.in);
        matricula=teclado.nextLine();
    }
     //Metodo toString lo sobre-escribo
    //@Override se pone para indicar al compilador que tu intención es sobreescribir
    //el método de la clase padre y si te equivocas (por ejemplo, al escribir el nombre
    //del método) y no estás realmente sobreescribiendo, el compilador dará un error.
    @Override
    public String toString(){
        return super.toString()+ " Matricula:"+this.matricula;
    }// cuando sobre-Escribo, al poder imprimir el toString, imprimira lo q heredo y
    // el atributo del objeto.
}
