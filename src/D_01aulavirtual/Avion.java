package D_01aulavirtual;

public class Avion extends Vehiculo1{
    // atributos
    private int cantPasajeros;
    // metodos constructores
    public Avion(String fabricante, String matricula, int anioFabricacion,
            float velocidadMaxima,int cantPasajeros) {
        super(fabricante,matricula,anioFabricacion,velocidadMaxima);
        this.cantPasajeros=cantPasajeros;
    }
    
    // Metodos setter y getter
    public int getCantPasajeros() {
        return cantPasajeros;
    }
    public void setCantPasajeros(int cantPasajeros) {
        this.cantPasajeros = cantPasajeros;
    }
    
    // Metodos
    public void aterrizar() {
        System.out.println("El avion esta aterrizando");
    }
    public void despegar() {
        System.out.println("El avion esta aterrizando");
    }
    
    @Override
    public String toString (){
        return super.toString() + "Cantidad Pasajeros"+cantPasajeros;
    }
    
//    public void prueba(){
//        System.out.println("Estoy probando el avion");
//    }
//     public void esOtraPrueba(){
//        super.prueba();
//    }
}
