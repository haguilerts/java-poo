package D_01aulavirtual;

public class TestVehiculo1 {
    public static void main(String[] args) {
        // Creo un objeto auto vacio
        Auto auto1=new Auto();
        auto1.setAnioFabricacion(2018);     // heredado
        auto1.setFabricante("Ford");        // heredado
        auto1.setMatricula("arg");          // heredado
        auto1.setModelo("Fiesta");          //de la clase
        auto1.setVelocidadMaxima(250);      // heredado
        auto1.setPatente("AB 1234 xz");     //de la clase
        
        //********************** una forma //**********************
        System.out.println("Auto1:"); 
        System.out.println(auto1);// imprime los datos ingresados x le metodo SET. que los 
                                  //tomara de toString adquerido
                                  
        //********************** segunda forma //**********************
        Auto auto2=new Auto("Fiat","arg",2018,220,"Punto","AB 9876 VV");
        System.out.println("Auto2:");
        System.out.println(auto2);
                System.out.println("*****************");
        auto1.acelerar(100);
        auto1.tocarBocina();
        auto1.apagar(); 
                System.out.println("******************");
        // el metodo de la clase auto
        // Crear un objeto de la clase avion
        Avion avion1=new Avion("Boeing","arg",2017,950,150);
        System.out.println("Avion1:");
        System.out.println(avion1);
        // avion1.esOtraPrueba();
        
     }
}
