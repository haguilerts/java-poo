package D_01aulavirtual;

import java.util.Scanner;

public class TestVehiculo {
    public static void main(String[] args) {
        // Generar un objeto
        Vehiculo vehiculo1=new Vehiculo();
        // Generar otro objeto con el constructor c/argumentos
        Vehiculo vehiculo2=new Vehiculo("Ford","Fiesta","Nafta",1600);
        // Ver el estado de los objetos
        System.out.println("Vehiculo1:" + vehiculo1);
        System.out.println("Vehiculo2:"+vehiculo2);
            System.out.println("*********** arrancar, acelerar, frenar, apagar **********");
        vehiculo2.arrancar();
        vehiculo2.acelerar(20);
        vehiculo2.acelerar(20);
        vehiculo2.frenar(40);
        vehiculo2.apagar();
        
//       // Ingresar datos desde teclado
//        Scanner teclado= new Scanner(System.in);
//        Vehiculo vehiculo3=new Vehiculo();
//            System.out.println("Ingrese la marca del vehiculo");
//        vehiculo3.setMarca(teclado.nextLine());
//            System.out.println("Ingrese la modelo del vehiculo");
//        vehiculo3.setModelo(teclado.nextLine());
//            System.out.println("Ingrese el combustible del vehiculo");
//        vehiculo3.setCombustible(teclado.nextLine());
//            System.out.println("Ingrese la cilindrada del vehiculo");
//        vehiculo3.setCilindrada(teclado.nextInt());
//            System.out.println("Vehiculo3: "+vehiculo3);
//        System.out.println("------------------------------------");
        
//        // Voy a mostrar los atributos del objeto a traves del gett
//        System.out.println("Vehiculo 3 marca:"+vehiculo3.getMarca());
//        System.out.println("Vehiculo 3 modelo:"+vehiculo3.getModelo());
//        System.out.println("Vehiculo 3 combustible:"+vehiculo3.getCombustible());
//        System.out.println("Vehiculo 3 cilindrada:"+vehiculo3.getCilindrada() );
        
    // Genero un nuevo objeto
        System.out.println("**********************:::::::::::::::::::");
    Vehiculo vehiculo4=new Vehiculo("Ford","Fiesta","Nafta",1600);
        System.out.println("Vehiculo4:"+vehiculo4);
        
        vehiculo4.arrancar();
        vehiculo4.acelerar(50);
        vehiculo4.acelerar(50);
        vehiculo4.acelerar(50);
        System.out.println("Velocidad actual:"+vehiculo4.getVelocidad());
        vehiculo4.acelerar(150);
        vehiculo4.frenar(100);
        vehiculo4.frenar(200);
        

    }
}
