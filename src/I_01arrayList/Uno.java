package I_01arrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
/*
Un arrayList es na coleccion de Java que implementa(ejecuta, hace) la interface List
Y se utiliza pra implementar listas
Cuando se crea un ArrayList se crea con un tamaño x defecto
en forma dinamica se puede agregar mas elementos
El Array tiene una cantidad definida
UN ArrayList solo puede guardar Objetos, no datos primitivos


*/
public class Uno {
    public static void main(String[] args) {
        // Creo una ArrayList vacio con tamaño x defecto
            List<String> a1= new ArrayList<>();
            a1.add("brandon");
        // Creo un ArrayList de cierta cantidad de posiciones 3 elementos
            ArrayList<String> a2= new ArrayList<>(3); // limite maximo es 3
                    // Creo un ArrayList desde otra coleccion existente
                    // 
        /*Java:Vector.add();
            Método que nos permite añadir elementos a un Vector. 
          Bien sea añadir elementos en la parte de atrás del Vector,
          con .add(E element), o en una posicion en concreto,
          con .add(int index, E element). */
        
            a2.add("Jose");
            a2.add("Maria");
            a2.add("Beatriz");
        // a2.add("Pablo");
            ArrayList<String> a3= new ArrayList<>(a2);// voy tomar los lim de a2.
            System.out.println("---------- lista size ------------");// el listado size calcula cantidad
        // Cantidad de elementos de una lista size()
            System.out.println("Cantidad de elementos de a1:"+a1.size() );
            System.out.println("Cantidad de elementos de a2:"+a2.size() );// imprime cantidades, este caso 3
            System.out.println("Cantidad de elementos de a3:"+a3.size() );// imprime cantidad , en este caso tambien es 3.
            
        for (int i = 0; i < a2.size(); i++) { // con el for recorro la lista            
            System.out.println("Contenido de Arrayist a2:"+a2.get(i)); // atras ves del metodo get, imprimo los contenidos de la lista.
        }
            System.out.println("----------- Iterator --> ´hasNext()´,´next()´---------");
            
        // Iterar un ArrayList
        // Crear un objeto llamado iterador del tipo Iterator
        // Tiene 2 metodos principales 
        // Permite el acceso secuencial a los objetos.
        // El metodo se llama .iterator() del ArrayList
        // Siempre puedo recorrer ua coleccion con iterator
                       
            /*  Un Iterador es un objeto que nos permite recorrer una lista y 
                presentar por pantalla todos sus elementos.
                Dispone de dos métodos clave para realizar esta operación hasNext() y next(). 
            
             En java es una interfaz denominada Iterator. Esta formado por 3 métodos:
              i) boolean hasNext(): retorna true en caso de haber mas elementos y false en caso de llegar al final de iterator
             ii) Object next(): retorna el siguiente elemento en la iteración
            iii) void remove(): remueve el ultimo elemento devulto por la iteración
            
            */
            
        // Para movernos usar metodo .next()
        // Hace 2 operaciones, obtiene el elemento sobre el 
        // y mueve el puntero al proximo elemento
        // .hasNext() devuelve un booleano
        // Si hay u proximo elemento devuelve TRUE
        Iterator<String> iter2=a2.iterator();
                System.out.println("///////////// inicio1 hasNext ***********");
                
            while(iter2.hasNext()){// miesntras q la lista lo recorra q..
                System.out.println("Elemento a2:"+iter2.next());// imprime toda la lista
            }
                System.out.println("///////////// fina1 hasNext ***********");

            System.out.println("///////////// inicio2 hasNext ***********");
            // Hago que vuelva a apuntar al primer elemento.
            iter2=a2.iterator();
            while(iter2.hasNext()){
                System.out.println("Elemento a2:"+iter2.next());
            }
            System.out.println("///////////// fina2 hasNext ***********");
        
        // Recorrer un lista con for each
        System.out.println("----------Iterar con for each------------");
        //for each : debemos indicar el dato que almacena la lista, en este caso ´String´, 
        //luego debemos declarar una variable pivote ´elemento´, finalmente dos puntos (:) y la lista que vamos a recorrer
        for (String elemento : a2) {
            System.out.println(elemento);
        }
        System.out.println("---------------- a3 3 elemtos (.add();) --------------------");
        
        System.out.println("Agregar elementos");
        a3.add("Silvana");
        a3.add("Pedro");
        a3.add("Beatriz");
        Iterator<String> iter3=a3.iterator();
        while(iter3.hasNext()){
            System.out.println("Elemento a3:"+iter3.next());
        }
        
        System.out.println("-------------añado a a3 dos datos ---------------------");
        // Que pasa si quiero agregar un elemento en una determinada posicion
        // Debo indicar en que posicion e la lista lo quiero agregar
        a3.add(0, "Daniel");
        a3.add(6, "Dario");
        // Recorro y muestro con for each
        for (String elemento :a3){
            System.out.println("Arraylist a3:"+elemento);
        }
       // a3.add(11, "Juan"); me da error 
        System.out.println("============ replazando valores (.set) ==============");
        // Como reemplazar un elemnto de la lista
        // Voy a reemplazar a todos los elementos que contienen el nombre Beatriz
        // Debo conocer en que posicion esta y utilizar el metodo set
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 danien, 1 jose, 2 maria, 3 beatris, 4 silvana, 5 pedro, 6 dario, 7 beatris
        }
        a3.set(3, "Romualda");
        a3.set(7,"Ramona");
        System.out.println("=====================================");
        a3.add(0,"Silvana");
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 Silvana, 1 danien, 2 jose, 3 maria, 4 Romualda, 5 silvana, 6 pedro, 7 dario, 8 Ramona
        }
        System.out.println("-----------Eliminar (.remove();)---------------------");
        // Eliminar un elemento del arraylist
        // El metodo de eliminar es remove
        a3.remove("Silvana");
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j)); }
        
        a3.remove("Silvana");
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j)); }
        
        a3.remove("Silvana");
        // Elimino un objeto en bases a la posicion
        a3.remove(4);
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 Daniel, 1 Jose, 2 Maria, 3 Romualda, 4 Dario, 5 Ramona
        }
        // a3.remove(10);  me da error
        System.out.println("------Ordenamiento de Arraylist (Collections.sort();) ------");
        // Ordenar el arrayList con un metodo alfanumeroco
        
        Collections.sort(a3);
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j)); 
            //0 Daniel, 1 Dario, 2 Jose, 3 Maria, 4 Ramona, 5 Romualda
        }
        
        a3.add("alberto");// agrego uno nuevo 
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j)); 
            // 0 Daniel, 1 Dario, 2 Jose, 3 Maria, 4 Ramona, 5 Romualda, 6 alberto
        }
        
        System.out.println("-----Ordeno la lista-------");
        Collections.sort(a3);
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 Daniel, 1 Dario, 2 Jose, 3 Maria, 4 Ramona, 5 Romualda, 6 alberto
        }
        a3.set(5, "Ana");
        a3.set(1, "Acosta");
        a3.set(3, "zamuray");
        
                for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 Daniel, 1 Acosta, 2 Jose, 3 zamuray, 4 Ramona, 5 Ana, 6 alberto
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++");
        Collections.sort(a3);
        for (int j = 0; j < a3.size(); j++) {
            System.out.println(j+" "+a3.get(j));
            //0 Acosta, 1 Ana, 2 Daniel, 3 Jose, 4 Ramona, 5 alberto, 6 zamuray
        }
    }
}
