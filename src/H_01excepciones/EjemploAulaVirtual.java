package H_01excepciones;

public class EjemploAulaVirtual {

    public static void main(String[] args) {
        int n, d, c;
        try {
            n = 10;
            d = 2;
            c = n / d;
            System.out.println(c);
        } catch (ArithmeticException e) {
            System.out.println("Division por cero");
        } finally {
            System.out.println("Final del primer catch");
            System.out.println("Las sentencias de este bloque"
                    + "se ejecutan siempre");
        }
        
        // Vamos a provocar otro tipo de excepcion
        // Convertir una letra en numero entero
        String valor = "f";
        try {
            int ohPorDiosNoLoHagas = Integer.parseInt(valor);
                System.out.println("valor:" + ohPorDiosNoLoHagas);
        } catch (NumberFormatException e) {
            System.out.println("E r r o r");
            System.out.println("No podemos convertir letras en numero");
        } finally {
            System.out.println("Final del 2do catch");
            System.out.println("Las sentencias de este bloque"
                    + "se ejecutan siempre");
        }
        
        System.out.println("-----------------------------------------");
        int i=0;
        String [] palabras={"hola","como","estas"};
        while (i<4){
            try {
                System.out.println(palabras[i]);
                //i++;
            } catch (ArrayIndexOutOfBoundsException e){
               // i++;
                System.out.println("Fuera de rango");
                System.out.println(e);
            } finally {
                i++;
                System.out.println("Esta sentencia se ejecuta siempre!!!");
            }
        
    }
}
}
