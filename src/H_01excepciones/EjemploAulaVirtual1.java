package H_01excepciones;

public class EjemploAulaVirtual1 {

    public static void main(String[] args) {
        int n, d, c;
        // Vamos a provocar otro tipo de excepcion
        // Convertir una letra en numero entero
        String valor = "6";
        int i = 0;
        String[] palabras = {"hola", "como", "estas"};
        try {
            n = 10;
            d = 0;
            c = n / d;
            System.out.println(c);
            int ohPorDiosNoLoHagas = Integer.parseInt(valor);
            System.out.println("valor:" + ohPorDiosNoLoHagas);
            while (i < 4) {
                System.out.println(palabras[i]);
                i++;
            }
        }
        catch (Exception e){
            System.out.println("Se produjo un error");
            System.out.println(e);
        }
//        catch (ArrayIndexOutOfBoundsException e) {
//            // i++;
//            System.out.println("Fuera de rango");
//            System.out.println(e);
//            // e.printStackTrace();
//        } 
//        catch (ArithmeticException e) {
//            System.out.println("Division por cero");
//        } catch (NumberFormatException e) {
//            System.out.println("E r r o r");
//            System.out.println("No podemos convertir letras en numero");
//        }  
        finally {
            System.out.println("Las sentencias de este bloque"
                    + "se ejecutan siempre");
        }
    }
}
