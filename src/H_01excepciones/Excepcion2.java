package H_01excepciones;

public class Excepcion2 {
    public static void lanzaExcepcion() throws Exception{
        System.out.println("Este metodo lanza una excepcion");
        try {
            throw new Exception();
        } catch (Exception e){
            System.out.println("Excepcion atrapada en el metodo");
            throw e;
        }
    }       
        
    public static void main(String[] args) {
        try { 
            lanzaExcepcion();
        } catch (Exception ex) {
            // Logger.getLogger(Excepcion2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Excepcion atrapada en el main");
        }
    }
}
