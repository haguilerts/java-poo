package H_01excepciones;

public class Excepcion1 {
    String s1="";
    String s2="";
    
    public void cargarNumeros(){
        s1="12";
        s2="0";
    }
    
    public void calculo() throws Exception{
        int n,c,d;
        // Metodo que convierte string a enteros
        n=Integer.parseInt(s1);
        d=Integer.parseInt(s2);
        c=n/d;
        System.out.println("Metodo calculo");   // No llega a ejecutarse
        // Convierte diferentes tipos de datos a string
        System.out.println(String.valueOf(c));
    }
    public static void main(String[] args) throws Exception{
        Excepcion1 obj=new Excepcion1();
        System.out.println("Linea1");
        obj.cargarNumeros();
        System.out.println("Linea2");
        obj.calculo();
        System.out.println("Linea3");
    }
    
}
