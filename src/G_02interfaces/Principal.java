package G_02interfaces;

// Implemento la interfce Operaciones
public class Principal implements Operaciones{
 
    // Sobre-escribo los metodos
    public double suma(){
        return Operaciones.NUMERO1+Operaciones.NUMERO2;
    }
     public double resta(){
        return Operaciones.NUMERO1-Operaciones.NUMERO2;
    }
     public double multiplicacion(){
        return Operaciones.NUMERO1*Operaciones.NUMERO2;
    }
      public double division(){
        return Operaciones.NUMERO1/Operaciones.NUMERO2;
    }
    
      public static void main(String[] args) {
        Principal p=new Principal();
          System.out.println("La suma es:"+p.suma());
          System.out.println("La resta es:"+(float) p.resta());
          System.out.println("La multi es:"+p.multiplicacion());
          System.out.println("La division es:"+p.division());
    }
}
