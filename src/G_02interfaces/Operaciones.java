package G_02interfaces;

public interface Operaciones {
    // Atributos de interface
    // Deben ser final , lo toma x defecto. No puedo cambiar el valor
    // Debe ser static, o toma x defeto
    // Deben ser public, l toma x defecto
    double NUMERO1=3.6;
    public static final double NUMERO2=5.9;
    // Los metodos son abstractos o aclare o no
    // No tienen cuerpo y no se puede imlementar
    double suma();
    public abstract double resta();
    double multiplicacion();
    double division();
    
}
