package C_01laboratoriocaja;

public class PruebaCaja {
    public static void main(String[] args) {
        //Creo un objeto del tipo caja
        Caja caja1=new Caja(3,2,6);
        caja1.CalcularVolumen();
        caja1.estado();
        // creo otra caja con el constructor vacio
        Caja caja2=new Caja();
        caja2.estado();
        System.out.println("------------Caja3-------------");
        // crear otra caja con el constructor vacio
        Caja caja3=new Caja();
        caja3.setAlto(5);
        caja3.setAncho(10);
        caja3.setProfundo(3);
        caja3.estado();
        caja3.CalcularVolumen();
        System.out.println("---Accedo a los atributos mediante get---");
        System.out.println("El alto de la caja3 es:"+caja3.getAlto());
        System.out.println("El ancho de la caja3 es:"+caja3.getAncho());
        System.out.println("La profundidad de la caja3 es:"+caja3.getProfundo());
        
    }
}
