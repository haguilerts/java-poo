package C_01laboratoriocaja;

public class Caja {
// atributos de la clase
    private int alto;
    private int profundo;
    private int ancho;

    // Constructores
    // Constructor vacio
   Caja(){
       
   }
    // Constructor con 3 atributos
   Caja(int ancho, int alto, int profundo){
       this.ancho=ancho;
       this.profundo=profundo;
       this.alto=alto;
   }
   // Metodos getters y setters
   public void setAncho(int ancho){
       this.ancho=ancho;
   }
   public void setProfundo(int profundo){
       this.profundo=profundo;
   }
   public void setAlto (int alto){
       this.alto=alto;
   }
   public int getAncho(){
       return this.ancho;
   }
   public int getProfundo(){
       return this.profundo;
   }
   public int getAlto(){
       return this.alto;
   }
   
    // metodos de la clase
   void CalcularVolumen(){
       System.out.println("El volumen del cubo es:"+(alto*ancho*profundo));
   }
   
   void estado(){
       System.out.println("El ancho es:"+ancho);
       System.out.println("El alto es:"+alto);
       System.out.println("La profundidad es:"+profundo);
   }
   
}
