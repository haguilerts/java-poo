
package A_01estructuras;

import java.util.Random;
import java.util.Scanner;

public class Estructuras {
         static int variableGlobal=0;    // esta es una variable global
          static Scanner teclado1 = new Scanner(System.in);
    public static void main(String[] args) {
////////////////////////////////////////////////////  Clase 1 ////////////////////////////////////////////////////
System.out.println("******************************** clase 1 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//         // TODO code application logic here
//        // Esto es un comentario de una sola linea.
//        
//        /*
//        Esto es un bloque de comentarios
//        Aqui puedo escibir que no se ejecuta
//        
//        */
//        System.out.println("Esto es un texto"); // Imprime por consola
//        // Lenguaje es Case Sensitive
//        // sout + tab
//        System.out.println("Curso de Codo a Codo");
//        System.out.print("1");  // Escribe en la misma linea
//        System.out.print("2");
//        System.out.print("3");
//        System.out.print("4");
//        System.out.println("Curso de Codo a Codo");
//        
//        // 2 clases de lenguajes con respecto a las variables
//        // lenguajes tipados Java C C++
//        // Lenguajes no tipados PHP, Phyton
//        
//        // Tipos de datos . Entero 
//        int a; // declarando una variable a del tipo entero
//        a=2;    // asignando valor
//        System.out.println (a);
//        int b=3; // declaro y asigno variable b
//        System.out.println(b);
//        
//        int c=a+b; // declaro c y asigno x medio de una operacion
//        System.out.println("c:"+c);
//        // int a; las variables solo se declaran 1 vez
//        // se pueden asignar valor muchas veces
//        a=20;
//        System.out.println(a);
//        System.out.println(2+2);    // muestra la suma de la operacion
//        System.out.println("2"+2);  // muestra 22
//        System.out.println("a+b:"+a+b);  // concatena el valor de a y b
//        System.out.println(a+b);
//        System.out.println("a+b:"+(a+b));
//       
//        // Variables para texto
//        // Variables String
//        String m="Perro";   // Declarando y asignando
//        String n="ladra";
//        System.out.println(m+n);    // Concateno variables del tipo String
//        System.out.println(m+" "+n);
//        System.out.println(m+" que "+n);
//        
//        // Variables tipo char
//        char p=65;          // Codigo ASCII de A
//        System.out.println(p);
//        char q=67;          // Codigo ASCII de C
//        System.out.println(p+" "+q);    // Muestro A y C
//        p+=32;              // p=p+32
//        q+=32;
//        System.out.println(p+" "+q);    // Muestro a y c
 
///////////////////////////////////////////////////  Clase 2 ////////////////////////////////////////////////////
System.out.println("******************************** clase 2 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // TODO code application logic here
//               // Variables Logicas o boolean
//               // Declarar una variable
//               // tipo nombre_variable valor
//       boolean x = true;
//        System.out.println(x);
//        x = false;
//        System.out.println(x);
//        System.out.println("x");
//        
//        // Variables tipo float 32 bits
//        float fl=9.75f;
//        System.out.println(fl);
//        
//        // Variables tipo double 64 bits
//        double dl=9.75;
//        System.out.println(dl);
//        
//        fl=10;
//        dl=10;
//        System.out.println(fl/3);
//        System.out.println(dl/3);
//        fl=100;
//        dl=100;
//        System.out.println(fl/3);
//        System.out.println(dl/3);
//        
//        // Operadores relacionales
//        // >  >= < <=  ==  !=
//        System.out.println("----------------------------------");
//        // Operadores incrementales
//        // Suma 1 a la variable
//        int nro1= 5;
//        int nro2=7;
//        System.out.println(nro1);
//        System.out.println(nro2);
//        nro1++; // Incremento en 1
//        System.out.println(nro1);
//        nro2--; // Decremento en 1
//        System.out.println(nro2);
//        
//        // Sumar 5 a la variable nro1
//        nro1+=5;    // nro1=nro1+5
//        System.out.println(nro1);
//        // Restar 5 a la variable nro1
//        nro1-=5;    // nro1=nro1-5
//        System.out.println(nro1);
//        // Multiplicar * 5
//        nro1*=5;    //  nro1=nro1*5
//        System.out.println(nro1);
//        // Dividir  / 5
//        nro1/=5;    // nro1=nro1/5
//        System.out.println(nro1);
//        System.out.println("-------------------------------");
//        // Precedencia y procedencia del operador ++ y --
//        System.out.println(nro1);       //6
//        System.out.println(nro1++);     //6
//        System.out.println(nro1);       //7
//        System.out.println(++nro1);
//        
//        // Operador resto
//        System.out.println(100%3);
//        System.out.println(200%6);
//        
//        // Declaracion de Constantes
//        // Se deben definir el tipo de dato
//        // Se usa nombre en mayuscula
//        // Solo puedo asignar valor en el momento que declaro
//        // Se debe usar la palabra
//        
//        final float PI= 3.14f;
//        System.out.println(PI);
//        // PI++; error no se puede modificar su valor
//        
//        // Operadores logicos
//        /*
//            Tabla de Verdad
//            X       Y       OR      AND
//            F       F       F       F
//            F       V       V       F
//            V       F       V       F
//            V       V       V       V
//        */
//        //      && and o y
//        //      || or  o o
//        
//        boolean log1=true;
//        boolean log2=false;
//        System.out.println(log1 && log2);       // false
//        System.out.println(log1 || log2);       // true
//        
//        System.out.println(nro1 == 8);          // true
//        System.out.println(nro1 == 9);          // false
//        
//        System.out.println(nro1 != 8);          // false
//        // Negacion de variable logica
//        System.out.println(!log2);          // true
//        System.out.println(log2);           // false
//        System.out.println(!!log2);         // false
//        
//        String texto ="hola";
//        // NO SE PUEDE HACER!!!
//        // No se puede comparar texto con el operador == o !=
//        
//        //System.out.println(texto=="hola");  // esta mal!!!!
//        
//        // Para comparar Strings el metodo equals() de la clase String
//        System.out.println(texto.equals("hola"));   // true
//        System.out.println(texto.equals("Hola"));   // false
//        System.out.println(texto.equalsIgnoreCase("Hola"));   // true
//        System.out.println(texto.equalsIgnoreCase("HoLa"));   // true
//        
//        // Operadores ternarios ?
//        // Son aquellos que necesitan 3 operandos
//        // expresionConValorBooleano ? expresion1 : expresion2
//            System.out.println(nro1<=1 ? "Es menor": "Es mayor");
//            System.out.println(nro1);       // 8
//        
//        int s=11;
//        String resultado = (s ==10) ? "Verdad": "Falso";
//            System.out.println(resultado);
//        // Estructura condicional IF
//        System.out.println("=============================");
//        x=true;
//        if (x) {
//            // Si la expresion es verdadera
//            // Se ejecuta estas intrucciones
//            System.out.println("Hoy es jueves");
//            System.out.println("Curso Codo a Codo");
//        }
//        
//        // Estructura IF Else
//        if (!x){
//            // Si es verdadero
//            System.out.println("Sali x verdadero");
//        } else {
//            // Si es falso
//            System.out.println("Sali x falso");
//        }
//        // IF abreviado
//        if (x) System.out.println("X Es verdadero");
//            
//

///////////////////////////////////////////////////  Clase 3 ////////////////////////////////////////////////////
System.out.println("******************************** clase 3 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // sout + tab
//        // Estructura de seleccion
//        // Estructura switch case
//        int nro=10;
//        switch (nro){
//            case 1:
//                System.out.println("nro vale1");
//                break;
//            case 2:
//                System.out.println("nro vale2");
//                break;
//            case 3:
//                System.out.println("nro vale3");
//                break;
//            case 4:
//                System.out.println("nro vale4");
//                break;
//            case 5:
//                System.out.println("nro vale5");
//                break;
//            case 6:
//                System.out.println("nro vale6");
//                break;
//            case 7:
//                System.out.println("nro vale7");
//                break;
//            case 8:
//                System.out.println("nro vale8");
//                break;
//            case 9: case 10:
//                System.out.println("El valor de nro es 9 o 10");
//                break;
//            default:
//                System.out.println("Nro no coincide");
//        }
//        
//        char caracter='A';
//        switch(caracter){
//            case 'a': case 'e': case 'i': case 'o': case 'u':
//            case 'A': case 'E': case 'I': case 'O': case 'U':
//                System.out.println("Es una vocal");
//                break;
//            //case 'b':
//            //    System.out.println("Es la letra b");
//            //    break;
//            default:
//                System.out.println("Caracter es una consonante");
//        }
//        /*
//        Ejercicio 1:
//        Dado el siguiente codigo
//        int nro1=100 nro2=500 nro3=250
//        Informar cual de los numeros es el mayor
//        
//        Ejercicio 2:
//        Dado:
//        int a=10 b=-2  c=5   // hay 2 numeros positivos y 1 negativo
//        Informar la multiplicacion de los dos numeros positivos
//        */
//        System.out.println("=====Ejercicio1=======");
//        int nro1=200, nro2=500, nro3=850;
//        if (nro1>nro2) {
//            // nro1>nro2
//            if (nro1>nro3){
//                //nro1>nro3
//                System.out.println("El nro1 es el mayor");
//            }else {
//               // nro3>nro1
//                System.out.println("El nro3 es el mayor");
//            }
//        } else {
//            // nro2>nro1
//            if (nro2>nro3){
//                // nro2 > nro3
//                System.out.println("El nro2 es el mayor");
//            }else {
//                // nro3 > nro2
//                System.out.println("El nro3 es el mayor");
//            }
//        }
//        System.out.println("==== Ejercicio 2======");
//        int a=10, b=2,  c=-5;
//        if (a<0){
//            // a es negativo
//            System.out.println("b*c:"+b*c);
//        }else {
//            if (b<0){
//                // b es negativo
//                System.out.println("a*c:"+a*c);
//            }else {
//                // c es negativo
//                System.out.println("a*b:"+a*b);
//            }
//        }
//        System.out.println("====Ejercicio 2 bis======");
//        if (a<0) System.out.println("b*c:"+b*c);
//        if (b<0) System.out.println("a*c:"+a*c);
//        if (c<0) System.out.println("a*b:"+a*b);
//        // Ejercicio 3
//        // Dado el siguiente codigo:
//        // String usuario="pepito", clave="123";
//        // Informar los siguientes casos
//        // si usuario=pepito y clave=123 informar "bienvenido Pepito"
//        // si el usuario=pepito pero la clave no es 123, informar 
//        // "el usuario no coincide con la clave"
//        // si el usuario no es pepito, informar
//        // "el usuario no existe"
//        System.out.println("==== Ejercicio3 ======");
//        String usuario="pepito";
//        String clave="456";
//        if (usuario.equals("pepito")){
//            if (clave.equals("123")){
//                System.out.println("Bienvenido Pepito");
//            } else {
//                System.out.println("El usuario no coincide con la clave");
//            }
//        } else {
//            System.out.println("El usuario no existe");
//        }
//        if (!usuario.equals("pepito")) System.out.println("El usuario no existe");
//        if (usuario.equals("pepito") && clave.equals("123"))
//            System.out.println("Bienvenido Pepito");
//        if (usuario.equals("pepito") && !clave.equals("123"))
//            System.out.println("El usuario no coincide con la clave");


///////////////////////////////////////////////////  Clase 4 ////////////////////////////////////////////////////
System.out.println("******************************** clase 4 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
//        //https://drive.google.com/drive/folders/1X6b2M7pQy0bDAnonezSS2MXTWtVzZjeh
//        int elMasGrande=0;
//        elMasGrande=devolverMayor(300,400,500);
//        recuadro();     // invoco al procedimiento recuadro
//        System.out.println("El nro mas grande:"+elMasGrande);
//        recuadro();     // invoco al procedimiento recuadro
//        System.out.println("El nro mas grande"+devolverMayor(23,45,12));
//        recuadro();     // invoco al procedimiento recuadro
//        // La cantidad de parametros debe coincidir en la invocacion
//        // como en la declaracion
//        // System.out.println("El nro mas grande:"+devolverMayor(12,3));
//        // Debe coincidir los tipos!!!!
//        //System.out.println("El nro mas grande:"+devolverMayor(8.5,9,3));
//        // num1=88; da error xq es variable local de la funcion
//        variableGlobal=22;      // vale xq es global
//        // Invoco a validar
//        recuadro();
//        recuadro();
//        validar("JUancito","456");
//        recuadro();
//        recuadro();
//        validar("pepito","123");
//        recuadro();
//    }
//
//// Funcion, ambito donde se puede definir
//    public static int devolverMayor(int num1, int num2, int num3) {
//        System.out.println("=====Comienzo de la funcion=======");
//        // elMasGrande=15; variable local del main
//        int mayor = 0;
//        if (num1 > num2) {
//            // nro1>nro2
//            if (num1 > num3) {
//                //nro1>nro3
//                System.out.println("El nro1 es el mayor");
//                mayor = num1;
//            } else {
//                // nro3>nro1
//                System.out.println("El nro3 es el mayor");
//                mayor = num3;
//            }
//        } else {
//            // nro2>nro1
//            if (num2 > num3) {
//                // nro2 > nro3
//                System.out.println("El nro2 es el mayor");
//                mayor = num2;
//            } else {
//                // nro3 > nro2
//                System.out.println("El nro3 es el mayor");
//                mayor = num3;
//            }
//        }
//        System.out.println("====Fin de la Funcion=====");
//        variableGlobal=33;  // lo puedo hacer xq es variable global
//        return mayor;
//    }
//    // Defino un procedimiento
//    public static void recuadro(){
//        System.out.println("**********************");
//    }
//    
//    // Declaro procedimiento validar
//    public static void validar(String usuario, String clave) {
//        if (!usuario.equals("pepito")) System.out.println("El usuario no existe");
//        if (usuario.equals("pepito") && clave.equals("123"))
//            System.out.println("Bienvenido Pepito");
//        if (usuario.equals("pepito") && !clave.equals("123"))
//            System.out.println("El usuario no coincide con la clave");  

///////////////////////////////////////////////////  Clase 5 ////////////////////////////////////////////////////
System.out.println("******************************** clase 5 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// // Estructuras repetitivas
//        // Estructura While
//        int a = 1;      // declarando la variable a
//        // int a=20;
//        System.out.println("-----Inicio del While-----");
//        while (a <= 10) {
//            // instrucciones que se repiten
//            System.out.println(a);
//            a++;
//        }
//        System.out.println("--- Fin del While-----");
//        System.out.println("a:" + a);
//        a = 1;
//        System.out.println("--- Inicio Do While ------");
//        do {
//            // Instrucciones que se repiten
//            // Por lo menos se ejecuta una vez
//            System.out.println(a);
//            a++;
//        } while (a <= 10);
//        System.out.println("---- Fin del Do While-----");
//        System.out.println("a:" + a);
//        // CUIDADO con los loops infinitos
////        a=1;
////        while(true){
////            // Instrucciones que se repiten
////            System.out.println(a);
////            a++;
////        }
//
//// Operador resto %
//        System.out.println(15 % 2);   // 1
//        System.out.println(-14 % 2);  // 0
//        System.out.println(-15 % 2);  // -1
//        System.out.println("/////////////////////////////");
//        
//         int f1=0;
//         
//       while (f1<10){
//            f1=f1+1;
////             int a1;
//            int c1= f1%2;
//            System.out.println(c1);// imprime 1010101010
//        } 
//           
//        
//            
//        
//// Estructura for
//        System.out.println("");
//        System.out.println("---Inicio de ciclo For----");
//        for (int x = 1; x <= 10; x++) {
//            // Instrucciones que se repiten
//            System.out.println(x);
//        }
//        System.out.println("---Fin del ciclo For----");
//        // System.out.println(x); la variable x es local a for
//        // utilizo una variable del tipo global
//        for (a=1; a<=10;a++){
//            System.out.println(a);// imprime del 1 al 10
//        }
//        System.out.println("a:"+a);
//        System.out.println("---------------------------");
//        for (a=10; a>=1;a--){
//            System.out.println(a);// imprimer del 10 al 1
//        }
//        System.out.println("---------------------------");
//        for (a=0; a<=100;a+=3.2400110){
//            System.out.println(a);
//        }
//        // Declaracion de Vectores o Arrays
//        // declaro un vector de 4 elementos
//        // del tipo entero con nombre numeros
//        int [] numeros= new int[4];
//        // declaro un vector llamado nombres
//        // del tipo String de 4 elementos
//        String [] nombres= new String[4];
//        // Debo acceder a los elementos del vector
//        // a traves del indice
//        numeros[0]=1;
//        nombres[0]="Juan";
//        numeros[1]=2;
//        nombres[1]="Laura";
//        numeros[2]=3;
//        nombres[2]="Jose";
//        numeros[3]=4;
//        nombres[3]="Dario";
//        // La primera posicion de un vector tiene indice 0
//        // La ultima posicion de un vector tiene indice N-1
//        System.out.println("--- Vectores-----");
//        // Mostrar a Laura 
//        System.out.println(numeros[1]+" "+nombres[1]);
//        
//        System.out.println("---Recorrer el vector----");
//        for(int b=0; b<4; b++) System.out.println(numeros[b]+" "+nombres[b]);// no necesito las {} ya q ejecuata en la misma linea 
//        
//        System.out.println("---Recorrer vector con metodo length");
//         for(int b=0; b<numeros.length; b++) System.out.println(numeros[b]+" "+nombres[b]);
//        
//        System.out.println("---Recorriendo vector con while---"); 
//        int r=0;
//        while(r<4){
//            System.out.println(numeros[r]+" "+nombres[r]);
//            r++;}


///////////////////////////////////////////////////  Clase 6 ////////////////////////////////////////////////////
System.out.println("******************************** clase 6 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // ----------------------------------------------------------------------------- 
//        // Laboratorio Parte 2 Estructura de control While
//        // ----------------------------------------------------------------------------- 
//        // Ejercicio 1 Parte 2
//        // Imprimir los números del 1 al 10 uno abajo del otro.
//        System.out.println("Ejercicio 1----------------");
//        int a = 1;
//        while (a <= 10) {
//            System.out.println(a);
//            a++;
//        }
//        // Ejercicio 2 Parte 2
//        // Imprimir los números del 1 al 10 salteando de a 2 uno abajo del otro.
//        System.out.println("Ejercicio 2------------");
//        a = 1;
//        while (a <= 10) {
//            System.out.println(a);
//            a += 2;
//        }
//        // Ejercicio 3 Parte 2
//        // Imprimir los números del 10 al 1 uno abajo del otro.
//        System.out.println("Ejercicio 3--------------------");
//        a = 10;
//        while (a > 0) {
////            System.out.println(a);
////            a--;
//            System.out.println(a--);
//        }
//        // Ejercicio 4 Parte 2
//        // Imprimir los números del 1 al 10 sin imprimir números 2,5 y 9 uno abajo 
//        // del otro.
//        // Requisito: se necesita tener conocimientos del operador AND (&&) y 
//        // del operador NOT  (!=).
//        System.out.println("Ejercicio 4-----------------------");
//      a = 1;
//        while (a <= 10) {
//            if (a != 2 && a != 5 && a != 9) {
//                System.out.println(a);
//            }
//            a++;
//        }
// // Ejercicio 5 Parte 2
//        // Imprimir los números del 1 al 30 sin imprimir números entre 
//        // el 10 y el 20 uno abajo del otro.
//        // Requisitos: se necesita tener conocimientos del operador OR (||) 
//        System.out.println("Ejercicio 5------------------------");
//        a = 1;
//        while (a <= 30) {
//            if (a < 10 || a > 20) {
//                System.out.println(a);
//            }
//            a++;
//        }
//// ----------------------------------------------------------------------------- 
//        //Laboratorio Parte 3 Estructura de control FOR 
//        // ----------------------------------------------------------------------------- 
//
//        //Ejercicio 1
//        // Imprimir los números del 1 al 10 uno abajo del otro.
//        System.out.println("Ejercicio 1------------------------");
//        for (int b=1;b<=10;b++){
//            System.out.println(b);
//        }
//        //Ejercicio 2
//        // Imprimir los números del 1 al 10 uno abajo del otro
//        // salteando de a dos.
//        System.out.println("Ejercicio 2------------------");
//         for (int b=1;b<=10;b+=2){
//            System.out.println(b);
//        }
//        // Ejercicio 3
//        // Imprimir los números del 10 al 1 uno abajo del otro.
//        System.out.println("Ejercicio 3------------------");
//        for (int b=10;b>=1;b--) System.out.println(b);
//        
//        // Haciendo un objeto de la clase Scanner llamado teclado
//        Scanner teclado= new Scanner(System.in);
//        
//        // Ingresar un numero entero
//        int n;
//        System.out.println("INgrese un numero entero:");
//        n=teclado.nextInt();
//        System.out.println("n:"+n);
//        
//        // Ingresar un numero tipo double
//        double d;
//        System.out.println("Ingrese un numero decimal:");
//        d=teclado.nextDouble();
//        System.out.println("d:"+d);
//        
//        // Ingreso de un String
//        String texto;
//        teclado.nextLine();     // limpieza del Buffer de teclado
//        System.out.println("Ingrese una palabra o texto:");
//        texto=teclado.nextLine();
//        System.out.println("texto:"+texto);


///////////////////////////////////////////////////  Clase 7 ////////////////////////////////////////////////////
System.out.println("******************************** clase 7 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
//        String cadena = "El que se fue a Sevilla perdió su silla y el que se fue al Torreón, su sillón";
//            System.out.println("El que se fue a Sevilla perdió su silla y el que se fue al Torreón, su sillón");
//        System.out.println("cadena.charAt(0)");
//        System.out.println(cadena.charAt(0)); // Nos devolvera E
//        
//        System.out.println("cadena.charAt(11)");
//        System.out.println(cadena.charAt(11)); //Nos devolvera u
//        
//        System.out.println("cadena.endsWith(\"n\")");
//        System.out.println(cadena.endsWith("n")); //Nos devuelve true si en caso hay un ene en la oracion.
//        
//        System.out.println("cadena.startsWith(\"e\")");
//        System.out.println(cadena.startsWith("e")); //Nos devuelve false, Java distingue entre mayusculas y minusculas
//        // 
//        System.out.println("equals");
//        System.out.println(cadena.equals("El que se fue a Sevilla perdió"
//                + " su silla y el que se fue al Torreón, su sillón ")); 
//        //+ "//Nos devuelve true
//        System.out.println("indexOf");
//        System.out.println(cadena.indexOf("fue")); //localiza la posición donde se encuentra "fue"
//        System.out.println("Length");
//        System.out.println(cadena.length()); // Nos devuelve la longitud: 77
//        System.out.println("Replace");
//        System.out.println(cadena.replace('a', 'e')); // Cambia todas las a por e
//        System.out.println("toLOwerCase");
//        System.out.println(cadena.toLowerCase()); //Transforma el String a minúsculas
//        System.out.println("toUpperCase");
//        System.out.println(cadena.toUpperCase()); //Transforma el String a mayúsculas
//        System.out.println(cadena);
//        System.out.println("--------------------------");
//        // Defino un vector y cargo numeros
//        int [] numeros = new int[5];
//        numeros[0]=23;
//        numeros[1]=42;
//        numeros[2]=12;
//        numeros[3]=88;
//        numeros[4]=8;
//        // numeros[5]=6;  da error xq solo son 5 numeros
//        for (int i = 0; i < 5; i++) {
//            System.out.println(numeros[i]);
//           
//        }
//        for (int i = 0; i < numeros.length; i++) {
//            System.out.println(numeros[i]);
//        }
//        
//            System.out.println("------------------------------");
//        Scanner teclado= new Scanner(System.in);
//            System.out.println("Ingrese cantidad de elementos:");
//            
//        int elem;
//        elem= teclado.nextInt();
//        int [] vector=new int[elem];    // declaro el vector
//            // Cargando elementos en el vector
//        for (int i = 0; i < vector.length; i++) {
//            
//            System.out.println("Ingrese un numero al vector:"+i);
//            vector[i]=teclado.nextInt();
//        }
//        System.out.println("==================");
//        // Mostrar el vector
//        for (int i = 0; i < vector.length; i++) {
//            System.out.println(vector[i]);
//        }


///////////////////////////////////////////////////  Clase 8 ////////////////////////////////////////////////////
System.out.println("******************************** clase 8 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
1) Codifique un programa en Java que permita saber si un número ingresado por teclado es
par o impar.
2) Codifique un programa en Java que permita el ingreso de un número, y a partir de ello,
mostrar los diez números siguientes al mismo. Utilizar estructuras iterativas.
3) Codifique un programa en Java que permita comparar tres números ingresados por
teclado, e informe cuál de ellos es el mayor.
4) Codifique un programa en Java que permita obtener la edad de una persona en meses, si
se ingresa su edad en años y meses. Ejemplo: Ingresando 3 años y 4 meses el programa
debe mostrar 40 meses.
5) Codifique un programa en Java que, ingresando una letra (número romano) (i,v,x,l,c,d,m)
diga su equivalente en números naturales. Utilice Switch-Case.
6) Codifique un programa en Java que permita ingresar dos números. Se le informará al
usuario si dichos números quiere sumarlos o restarlos. Si el usuario ingresa un 1 dichos
números se sumarán, y si ingresa 2, se restarán. La suma y la resta de dichos números
debe realizarse con dos métodos tipo función. En el caso de la suma, dicho método
recibirá como parámetros los dos números ingresados y devolverá la suma de los dos
números. En el caso de la resta se procederá de la misma manera, pero el método
devolverá la resta de los mismos.
*/

//     int numero=0;
//     // Declaro un objeto de la clase Scanner
//      Scanner teclado = new Scanner(System.in); 
//     // Para acceder al detalle de como estan realizadas las clases
//     // Nos ubicamos con el cursor arriba y apretamos control
//     // Aparece el menu contextual y haciendo click accedemos al codigo
//        System.out.println("Ingrese un numero entero:");
//        numero=teclado.nextInt();
//        recuadro();//********************************
//        for (int i = 0; i < 10; i++) {
//            System.out.println(i+numero);
//        }
//       recuadro();//******************************** 
//       recuadro();//********************************
//       recuadro("_");
//       recuadro("=");
//       recuadro("?",30);
//   
//       
//    }
//    
//    public static void recuadro(){
//        System.out.println("*****************************************");
//        // signirfica q solo va ser leido envocando el metodo. no retornara e ingresara nada. 
//    }
//    public static void recuadro(String simbolo){
//        for (int i = 0; i < 20; i++) {
//            System.out.print(simbolo);
//        }
//        System.out.println("");
//    }
//    
//    public static void recuadro(String simbolo,int cant){
//        for (int i = 0; i < cant; i++) {
//            System.out.print(simbolo);
//        }
//        System.out.println("");
//    // dentro del metod mail, pueden ir otros metodos y no hay erro.
//    

///////////////////////////////////////////////////  Clase 9 ////////////////////////////////////////////////////
System.out.println("******************************** clase 9 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        // 3 formas de declarar vectores, arrays, listas
//        // 1ra forma declaro numeros como vector pero no asigno la dimension
//        int[] numeros;
//        numeros = new int[3];
//        for (int i = 0; i < numeros.length; i++) {
//            // Si no asigno valores, Java lo inicializa con cero
//            System.out.println(numeros[i]);
//        }
//        // Que pasa si el vector es de String
//        String [] nombres;
//        nombres = new String[3];
//        for (int i = 0; i < nombres.length; i++) {
//            System.out.println(nombres[i]);
//            // Por defecto Java inicializa con null
//        }
//        
//        // 2da forma de declarar un vector
//        int[] numeros1 = new int[3];
//        String[] nombres1=new String[3];
//        for (int a = 0; a < numeros1.length; a++) {
//            System.out.println(numeros1[a]);
//            System.out.println(nombres1[a]);
//        }
//        
//        // 3ra forma de declaro y asigno
//        // Forma abreviada. Debo asignar en obligatoria
//        int[] numeros2 ={23,15,44};
//        for (int i = 0; i < numeros2.length; i++) {
//            System.out.println(numeros2[i]);
//        }
//        String[] nombres2={"Juan","Pedro","Maria"};
//        for (int i = 0; i < nombres2.length; i++) {
//            System.out.println(nombres2[i]);
//        }
//        
//        System.out.println("*************************************");
//        // Declaro un vector donde la dimension la ingreso x teclado
//        // Scanner teclado = new Scanner(System.in);
//        int cantElementos;
//                System.out.println("Ingrese la cant. de elementos del vector");
//            cantElementos=teclado1.nextInt();
//        int[] vector;
//            vector= new int[cantElementos]; // ingreso x teclado
//            for (int i = 0; i < vector.length; i++) {
//                System.out.println("Ingrese el elemento "+i+" del vector");
//                vector[i]=teclado1.nextInt();
//            }
//            
//            System.out.println("*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/");
//          
//        vector=cargarVector(vector);
//        mostrarVector(vector);
//        
//           System.out.println("//////////// declaro un vector de Strings ////////////////");
//        
//            //declaro un vector de Strings
//        String[] vector2= new String[cantElementos];
//        teclado1.nextLine(); // Limpiando el buffer de teclado
//        vector2=cargarVector(vector2);
//        mostrarVector(vector2);
//        // Pruebita de variables
//        int menor=55;
//        pruebaVariables(menor,vector2);
//        System.out.println(menor);
//        mostrarVector(vector2);
//        
//        
//    }
//    public static void pruebaVariables(int menor,String[] auxVector){
//        menor=99999;
//        auxVector[1]="Sampaoli";
//    }
//    
//    
//    
//    // Metodo que carga el vector
//    public static int[] cargarVector(int[] auxVector){
//        System.out.println("Estoy en el metodo cargarVector");
//        for (int i = 0; i < auxVector.length; i++) {
//            System.out.println("Ingrese el elemento "+i+" del vector");
//            auxVector[i]=teclado1.nextInt();
//        }
//    return auxVector;    
//    }
//    // Sobrecarga de Metodo
//    // Es el mismo metodo con una firma de parametros diferentes
//    public static String[] cargarVector(String[] auxVector){
//        System.out.println("Estoy en el metodo cargarVector de String");
//        for (int i = 0; i < auxVector.length; i++) {
//            System.out.println("Ingrese el elemento "+i+" del vector");
//            auxVector[i]=teclado1.nextLine();
//        }
//    return auxVector; 
//    }
//    
//    
//    // Mostrar contenido del vector
//    public static void mostrarVector(int[] auxVector){
//        System.out.println("Estoy en el metodo mostrarVector");
//        for (int i = 0; i < auxVector.length; i++) {
//            System.out.println(auxVector[i]);
//        }
//    }
//    // Sobrecarga de metodo
//     public static void mostrarVector(String[] auxVector){
//        System.out.println("Estoy en el metodo mostrarVector de String");
//        for (int i = 0; i < auxVector.length; i++) {
//            System.out.println(auxVector[i]);
//        }        

        
///////////////////////////////////////////////////  Clase 10 ////////////////////////////////////////////////////
System.out.println("******************************** clase 10 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
//// Objeto del tipo vector de 10 elementos enteros
//        int[] vector = new int[10];
//        // Objeto del tipo random o aleatorio
//        Random rnd= new Random();
//        // Cargar el vector con numeros aleatorios
//            for (int i = 0; i < vector.length; i++) {
//                // LLeno el vector con numeros aleatorios
//                // desde 0 al 500
//                vector[i]=rnd.nextInt(500);
//            }
//          System.out.println("****************************");
//        int valorMinimo,valorMaximo;
//            for (int i = 0; i < vector.length; i++) {
//                System.out.println(vector[i]);
//            }
//        valorMinimo=minVector(vector);
//        valorMaximo=maxVector(vector);
//        System.out.println("El valor mas pequeño es:"+valorMinimo);
//        System.out.println("El valor mas grande es:"+valorMaximo);
//        // Voy a mostrar los valores sin asignar a variable
//        System.out.println("El valor mas pequeño:"+minVector(vector));
//        System.out.println("El valor mas grande:"+maxVector(vector));
//        
//        // Promedio de los valores del vector
//        System.out.println("El promedio del vector:"+promedioVector(vector));
//        
//        // Declarar matrices 
//        int matriz [][]= new int[3][3];
//        matriz[0][0]=1;
//        matriz[0][1]=2;
//        matriz[0][2]=3;
//        // matriz[0][3]=4; no existe
//        System.out.println("Valor de una celda de la matriz:"+matriz[0][0]);
//        
//        
//        
//        
//        
//    }
//     public static int minVector(int[] auxVector) {
//        int minimo = auxVector[0];
//        for (int i = 1; i < auxVector.length; i++) {
//            if (minimo > auxVector[i]) {
//                minimo = auxVector[i];
//            }
//        }
//        return minimo;
//    }
//
//    // Funcion que recibe un Vector de enteros y devuelve el mayor
//    public static int maxVector(int[] auxVector) {
//        int maximo = auxVector[0];
//        for (int i = 1; i < auxVector.length; i++) {
//            if (maximo < auxVector[i]) {
//                maximo = auxVector[i];
//            }
//        }
//        return maximo;
//    }
//    // Metodo para hacer el promedio
//    public static float promedioVector(int[] auxVector){
//        float total=0f;    // acumulador donde sumo los valores
//        for (int i = 0; i < auxVector.length; i++) {
//            total+=auxVector[i];
//        }
//        return (total/auxVector.length);
//    }
//        public  void no hace nada (){

///////////////////////////////////////////////////  Clase 11 ////////////////////////////////////////////////////
System.out.println("******************************** clase 11 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Declaro un objeto del tipo vector
//        int[] vector = new int[10];
//        int aux;
//        //Declaro un objeto del tipo Random
//        Random rnd = new Random();
//        //Relleno el vector con numeros aleatorios del 0 al 499
//        for (int i = 0; i < vector.length; i++) {
//            vector[i] = rnd.nextInt(500);
//        }
//        
//        System.out.println("= = = MUESTRO VECTOR DESORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println("");
//        // ESTE METODO ES EL METODO X INSERCION!!!!
//        // Uso for anidados para ordenar de menor a mayor
//        // METODO DE INSERCION 
//        for (int i = 0; i < vector.length; i++) {
//            for (int j = i + 1; j < vector.length; j++) {
//                if (vector[i] > vector[j]) {
//                    aux = vector[i]; // guardo el elemento de i para no perderlo
//                    vector[i] = vector[j];
//                    vector[j] = aux;
//                }
//            }
//                System.out.println("ITERACION===> " + i);
//            for (int x = 0; x < vector.length; x++) {
//                System.out.print(+vector[x] + " ,");
//            }
//            System.out.println(" ");
//
//        }
//
//        System.out.println("= = = MUESTRO VECTOR ORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println("");
//        System.out.println("====================================");
//        System.out.println("====================================");
//        System.out.println("====================================");
//        //Relleno el vector con numeros aleatorios del 0 al 499
//        for (int i = 0; i < vector.length; i++) {
//            vector[i] = rnd.nextInt(500);
//        }
//        System.out.println("= = = MUESTRO VECTOR DESORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println(" Metodo de ordenamiento BURBUJA");
//        // Metodo de ordenamiento BURBUJA
//        for (int j = 0; j < vector.length; j++) {
//            for (int i = 0; i < vector.length - 1; i++) {
//                if (vector[i] > vector[i + 1]) {
//                    aux = vector[i];
//                    vector[i] = vector[i + 1];
//                    vector[i + 1] = aux;
//                }
//            }   
//            System.out.println("ITERACION===> " + j);
//            for (int x = 0; x < vector.length; x++) {
//                System.out.print(+vector[x] + " ,");
//            }
//            System.out.println(" ");
//        }
//        System.out.println("= = = MUESTRO VECTOR ORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println("");
//        System.out.println("====================================");
//        System.out.println("====================================");
//        System.out.println("====================================");
//        //Relleno el vector con numeros aleatorios del 0 al 499
//        for (int i = 0; i < vector.length; i++) {
//            vector[i] = rnd.nextInt(500);
//        }
//        System.out.println("= = = MUESTRO VECTOR DESORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println("");
//        // Metodo de ordenamiento BURBUJA 2
//        for (int i = 0; i < vector.length; i++) {
//            for (int j = 1; j < (vector.length - i); j++) {
//                if (vector[j - 1] > vector[j]) {
//                    aux = vector[j - 1];
//                    vector[j - 1] = vector[j];
//                    vector[j] = aux;
//                }
//            }
//            System.out.println("ITERACION===> " + i);
//            for (int x = 0; x < vector.length; x++) {
//                System.out.print(+vector[x] + " ,");
//            }
//            System.out.println(" ");
//        }
//        System.out.println("= = = MUESTRO VECTOR ORDENADO = = =");
//        for (int i = 0; i < vector.length; i++) {
//            System.out.print(+vector[i] + ", ");
//        }
//        System.out.println("");
//        System.out.println("====================================");
//        System.out.println("====================================");
//        System.out.println("====================================");


///////////////////////////////////////////////////  Clase 12 ////////////////////////////////////////////////////
//System.out.println("******************************** clase 12 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Ejercicio Repaso
Usar estructura repetitiva, if, switch, variable booleana)
a. Ingrese nombre y apellido del individuo
b. Permitir el ingreso de 2 numeros decimales
c. Presentar un menu de opciones de operaciones
    1. Sumar
    2. Restar
    3. Multiplicar
    4. Dividir
    0. Salir

d. Mostrar el resultado de la operacion elegida
    junto al nombre y apellido.
e. Seguir ingresando numeros hasta que se elija la opcion 0.salir
f. Controlar operaciones invalidas.

*/

//// Declarando variables
//        String nombre,apellido;
//        Scanner teclado = new Scanner(System.in);
//        float num1=0,num2=0,resultado=0;
//        int opcion=0;   // Opcion del menu
//        boolean salir=false;
//        // Ingreso de info
//        System.out.println("Ingrese el nombre:");
//        nombre=teclado.nextLine();
//        System.out.println("Ingrese el apellido");
//        apellido=teclado.nextLine();
//        while(!salir){
//            System.out.println("Ingrese el primer numero");
//            num1=teclado.nextFloat();
//            System.out.println("Ingrese el segundo numero");
//            num2=teclado.nextFloat();
//            System.out.println("Menu de operaciones");
//            System.out.println("1. Sumar");
//            System.out.println("2. Restar");
//            System.out.println("3. Multiplicar");
//            System.out.println("4. Dividir");
//            System.out.println("0. Salir");
//            opcion=teclado.nextInt();
//            switch (opcion) {
//                case 1:
//                    System.out.println("Sumar");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1+num2;
//                    System.out.println("El resultado es:"+resultado);
//                    break;
//                case 2:
//                    System.out.println("Restar");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1-num2;
//                    System.out.println("El resultado es:"+resultado);
//              
//                    break;
//                case 3:
//                    System.out.println("Multi");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1*num2;
//                    System.out.println("El resultado es:"+resultado);
//              
//                    break;
//                case 4:
//                    if (num2!=0){
//                    System.out.println("Dividir");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1/num2;
//                    System.out.println("El resultado es:"+resultado);
//                    } else {
//                        System.out.println("No se puede realizar la division");
//                    }
//                    break;
//                case 0:
//                    System.out.println("Salir");
//                    salir=true;
//                    break;
//                default:
//                    System.out.println("Opcion no valida");
//            }
//        }       
///////////////////////////////////////////////////  Clase 13 ////////////////////////////////////////////////////
//System.out.println("******************************** clase 13 ********************************");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 // declarar una variable entera
//        int num=10;
//        System.out.println(num);
//        
//        System.out.println("--------1 -------");
//            //Declaro un vector
//            int vector [] = new int [5];
//            vector [0]=55;
//            vector [1]=66;
//            vector [3]=77;
//            for (int i = 0; i < vector.length; i++) {
//                System.out.println(vector[i]);
//            }
//            
//        System.out.println("-------2 -----");
//            // Declaro otro vector
//            int vectorbis []= new int [5];
//            vectorbis[0]=99999;
//            System.out.println(vectorbis[0]);
//            for (int i = 0; i < vectorbis.length; i++) {
//                System.out.println(vectorbis[i]);
//            }
//            
//        System.out.println("--------3 -------");
//            vectorbis=vector;
//            for (int i = 0; i < vectorbis.length; i++) {
//                System.out.println(vectorbis[i]);
//            }            
//            System.out.println(vectorbis);
//            
//        System.out.println("----------4 ----------");
//            String nombres [] =new String[5];
//            nombres[0]="JUan";
//            nombres[1]="Pedro";
//            for (int i = 0; i < nombres.length; i++) {
//                System.out.println(nombres[i]);
//            }
//            System.out.println(nombres);
//
//
//
//// Declarando variables
//            Scanner teclado = new Scanner(System.in);
//            
//            String nombre,apellido;            
//            float num1=0,num2=0,resultado=0;
//            int opcion=0;   // Opcion del menu
//            boolean salir=false;
//        // Vectores para guardar la info
//            float numeros1[] = new float[5];
//            float numeros2[] = new float[5];
//            float resultados[] = new float[5];
//            int opciones[]=new int[5];
//        // Declaro variable para guardar el indice
//            int indice=0;
//        //Declaro y cargo un vector con las operaciones
//            String operaciones[]=new String[]{"Sumar","Restar","Multiplicar","Dividir"};
//        // Ingreso de info
//        System.out.println("Ingrese el nombre:");
//        nombre=teclado.nextLine();
//        System.out.println("Ingrese el apellido");
//        apellido=teclado.nextLine();
//        while(!salir){
//            System.out.println("Ingrese el primer numero");
//            num1=teclado.nextFloat();
//            System.out.println("Ingrese el segundo numero");
//            num2=teclado.nextFloat();
//            System.out.println("Menu de operaciones");
//            System.out.println("1. Sumar");
//            System.out.println("2. Restar");
//            System.out.println("3. Multiplicar");
//            System.out.println("4. Dividir");
//            System.out.println("0. Salir");
//            opcion=teclado.nextInt();
//            switch (opcion) {
//                case 1:
//                    System.out.println("Sumar");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1+num2;
//                    numeros1[indice]=num1;
//                    numeros2[indice]=num2;
//                    resultados[indice]=resultado;
//                    opciones[indice]=opcion;
//                    indice++;
//                    System.out.println("El resultado es:"+resultado);
//                    break;
//                case 2:
//                    System.out.println("Restar");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1-num2;
//                    numeros1[indice]=num1;
//                    numeros2[indice]=num2;
//                    resultados[indice]=resultado;
//                    opciones[indice]=opcion;
//                    indice++;
//                    System.out.println("El resultado es:"+resultado);
//              
//                    break;
//                case 3:
//                    System.out.println("Multi");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1*num2;
//                    numeros1[indice]=num1;
//                    numeros2[indice]=num2;
//                    resultados[indice]=resultado;
//                    opciones[indice]=opcion;
//                    indice++;
//                    System.out.println("El resultado es:"+resultado);
//              
//                    break;
//                case 4:
//                    if (num2!=0){
//                    System.out.println("Dividir");
//                    System.out.println("Nombre y apellido:"+nombre+" "+apellido);
//                    resultado=num1/num2;
//                    numeros1[indice]=num1;
//                    numeros2[indice]=num2;
//                    resultados[indice]=resultado;
//                    opciones[indice]=opcion;
//                    indice++;
//                    System.out.println("El resultado es:"+resultado);
//                    } else {
//                        System.out.println("No se puede realizar la division");
//                    }
//                    break;
//                case 0:
//                    System.out.println("Salir");
//                    salir=true;
//                    break;
//                default:
//                    System.out.println("Opcion no valida");
//            }
//        }
//        System.out.println("\tnumeros 1\tnumeros 2\tresultados\t operaciones");
//        for (int i = 0; i < indice; i++) {
//            System.out.println("\t"+numeros1[i]+
//                    "\t"+"\t"+numeros2[i]+
//                    "\t"+"\t"+operaciones[opciones[i]-1]+
//                    "\t"+"\t"+resultados[i]); 
//        }

///////////////////////////////////////////  FIN  //////////////////////////////////////////////////////////
    }
}
