package F_02herencia_abstracto_interface_aulavirtual3;

public class C0_Canario implements C1_Cantante{
    
//******************** Atributos  **********************************************
        private String tipo;
        
//******************** constructor  **********************************************
        public C0_Canario(){ }
        
//******************** metodos implementados ***********************************
    // debo sobre-escribir el metodo cantar, vosFina, vosAguda. se usa todo los metodos si o si
    @Override
    public void cantar(){
        System.out.println("Pio, pio, pio");
    }
    public void vosFina(String c){
        c="liiiiii, liiiiii, liiiiiiii,.....";
        System.out.println("Soy un alumno con la voz fina "+c);
    }
    public String vosAguda(){
        String cantare="Soy un alumno con la voz Aguda loooooo, looooooo, looooooo,.....";
        return cantare;
    }
//******************** main  **********************************************   
    public static void main(String[] args) {
        C0_Canario piolin=new C0_Canario();
        System.out.println("Soy un lindo canarito, llamado Piolin");
        piolin.cantar();
        String vos="mi vos fina";
        piolin.vosFina(vos);
        String a=piolin.vosAguda();
        System.out.println(a);
    }
    
}
