package F_02herencia_abstracto_interface_aulavirtual3;

public class Test {
    public static void main(String[] args) {
        
//        B0_Persona persona1=new B0_Persona();
//        //System.out.println("persona1:"+persona1);
//        persona1.obtener();// ingresa nombre y edad
//        System.out.println("persona1:"+persona1);
        // crear un objeto de la clase domicilio
        A0_Domicilio dom1=new A0_Domicilio("Cucha Cucha",2345,"","Caba","Caba");
        dom1.mostrar();
        // Creo una persona2 y pasarle el domicilio
//        B0_Persona persona2=new B0_Persona("Juan Perez",34,dom1);
//        System.out.println("persona2:"+persona2);
//        dom1=null;
//        System.out.println("persona2:"+persona2);
//        dom1.mostrar();
        System.out.println("*************************");
        A2_Alumno alumno2=new A2_Alumno("Juan Perez",34,"Ast9876",dom1);
        System.out.println("alumno2:"+alumno2);
        // dom1=null;
        // System.out.println("alumno2:"+alumno2);
        // dom1.mostrar();
        A0_Domicilio dom3=new A0_Domicilio("Av. de Mayo",835,"6 64","Caba","Caba");
        A2_Alumno alumno3=new A2_Alumno("Matias Quiroga",25,"Fis6498",dom3);
        System.out.println("alumno3:"+alumno3);
        // Tarea para el hogar realizar la clase motor
        // y modificar las clases Vehiculo, Automovil, Avion
        
////////////////////////////////////////////////////////////////////////////////////////////////        
        System.out.println("------Polimorfismo---------");
        // Crear una variable polimorfica
        B0_Persona pers; // ´pers´ es del tipo la clase persona de package: ´aulavirtuaal3´ q fue importado.
        pers = new A2_Alumno("Ruso Rodriguez",28,"Hist5698");
        pers.presentarse();
        pers=new A1_Docente("Profesor Jirafales",55,"Mat-6512");
        pers.presentarse();
        System.out.println("*****************************************");
        System.out.println("Vector del tipo persona------------");
        B0_Persona[] vectorPersona= new B0_Persona[3];
        vectorPersona[0]= new A2_Alumno("Carlos Tevez",32,"Idi7523");
        vectorPersona[1]= new A1_Docente("Homero Simpson",42,"Coc4233");
        vectorPersona[2]= new A2_Alumno("Lucas Alario",26,"Riv4999");
        // vectorPersona[2]=new A3_Artista();
        for (int i = 0; i < vectorPersona.length; i++) {
            // System.out.println("B0_Persona:"+vectorPersona[i]);
            vectorPersona[i].presentarse();
        }
        System.out.println("---recorrido con for each-------");
        for (B0_Persona pirulo : vectorPersona){
            pirulo.presentarse();
        }
        System.out.println("************************************");
        // Interface y polimorfismo
        pers.cantar();
        pers = new A2_Alumno("Ruso Rodriguez",28,"Hist1234");
        pers.cantar();
        // ---- Interfaces------
        A2_Alumno alu=new A2_Alumno();
        C0_Canario ave=new C0_Canario();
        A3_Artista actor=new A3_Artista();
        A1_Docente doce=new A1_Docente();
        System.out.println("--------------------------------");
        // llamo al metodo cantar desde el objeto.
        alu.cantar();
        ave.cantar();
        actor.cantar();
        doce.cantar();
        System.out.println("---------por medio del metodo HazleCantar----");
        hazleCantar(alu);   // le paso un alumno
        hazleCantar(ave);
        hazleCantar(actor);
        hazleCantar(doce);
        // Aunque son objetos de distinta naturaleza
        // todos implementan la interface Cantant
          
     }
    // Creo un metodo estatico que recibe como parametro 
    // un objeto de la clase cntante que es un interface
    // al ser un metodo estatico pertenece a la clase
    public static void hazleCantar(C1_Cantante interprete){
        interprete.cantar();
    }
    
    
}
