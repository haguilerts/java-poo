package F_02herencia_abstracto_interface_aulavirtual3;

import java.util.Scanner;

public class A2_Alumno extends B0_Persona{
//******************** Atributos  **********************************************
    private String matricula;
    
//******************** constructor  **********************************************  
    public A2_Alumno(){  }
    // Metodo constructor con 3 parametros
    public A2_Alumno(String nombre,int edad,String matricula){
        // super(nombre,edad);  // llamo al 2do constructor de B0_Persona
        super(edad,nombre);     // llamo al ul 3er constructor de B0_Persona
        this.matricula=matricula;
    }    
    // Metodo constructor con 4 parametros, heredados e instaciado
    public A2_Alumno(String nombre,int edad,String matricula, A0_Domicilio domicilio){
        super(nombre,edad,domicilio);
        this.matricula=matricula;
    }
//******************** grt and set **********************************************     
    public void setMatricula(String matricula){
        this.matricula=matricula;    }
    public String getMatricula(){
        return this.matricula;    }

//******************** metodos  **********************************************  
    // Metodo toString lo sobre-escribo
    public String toString(){
        return super.toString()+ " Matricula:"+this.matricula;
    }
        
//******************** metodos  **********************************************  
    
    @Override
    public void obtener(){
        // Accedo al metodo obtener de la superclase
        // con el operador super
        super.obtener();
        System.out.println("Ingrese la matricula del alumno:");
        Scanner teclado=new Scanner(System.in);
        matricula=teclado.nextLine();
    }
//********************* un metodo abstracto ************************************
    // Implemento el metodo abstracto heredado De la clase B0_Persona
    public void metodoAbstracto(){ }
    
    // Implemento el metodo presentarse de la clase B0_Persona
    // Sobreescribo el metodo
    @Override
    public void presentarse(){
        System.out.println("Soy el alumno:"+getNombre()); // heredado de la clase persona 
        System.out.println("Mi matricula es:"+getMatricula()); // pertenece a la clase alumno 
    }
    
    // Implemento el metodo cantar de la interface cantante y se implemneta todo los metodos si o si
    public void cantar(){
        System.out.println("Soy un alumno que canta. la, la, la,.....");
    }
    public void vosFina(String cantare){
        cantare="li, li, li,.....";
        System.out.println("Soy un alumno con la voz fina "+cantare);
    }
    public String vosAguda(){
        String cantare="Soy un alumno con la voz Aguda lo, lo, lo,.....";
        return cantare;
    }
    
}
