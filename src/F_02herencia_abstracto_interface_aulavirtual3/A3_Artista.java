package F_02herencia_abstracto_interface_aulavirtual3;

public class A3_Artista extends B0_Persona{
    // metodo constructor
    public A3_Artista(){ }
    // metodos
    @Override
    public void presentarse(){
        System.out.println("Soy un artista de nivel internacional.....");
    }
    // implementa cantante
    @Override
    public void cantar(){
        System.out.println("Soy un cantante, larala, larala, larala");
    }
    public void vosFina(String c){
        c="liiiiii, liiiiii, liiiiiiii,.....";
        System.out.println("Soy un alumno con la voz fina "+c);
    }
    public String vosAguda(){
        String cantare="Soy un alumno con la voz Aguda loooooo, looooooo, looooooo,.....";
        return cantare;
    }
}
