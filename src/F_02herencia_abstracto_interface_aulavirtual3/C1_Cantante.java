package F_02herencia_abstracto_interface_aulavirtual3;

public interface C1_Cantante {
    // public 
    // los metodos van ser todos abstractos, no va ser necesario poner la palabra clave ´abstract´
    public void cantar();   
    
    public void vosFina(String cantar);
    
    public String vosAguda();
    
}
