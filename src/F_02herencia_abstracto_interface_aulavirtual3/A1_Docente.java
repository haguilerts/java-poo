package F_02herencia_abstracto_interface_aulavirtual3;

import java.util.Scanner;

public class A1_Docente extends B0_Persona{// A1_Docente hereda(extends) de B0_Persona
    // Atributos
    private String legajo;
    
//******************** Metodos constructores *******************************
    public A1_Docente(){} 
    // usao la herencia para heredar nombre, edad para el constructor.
    public A1_Docente(String nombre,int edad,String legajo){
        // super(nombre,edad);  // llamo al 2do constructor de B0_Persona
        super(edad,nombre);     // llamo al ul 3er constructor de B0_Persona
        this.legajo=legajo;
    }      
    public A1_Docente(String nombre,int edad,String legajo, A0_Domicilio domicilio){
        super(nombre,edad,domicilio);
        this.legajo=legajo;
    }
 //******************** Metodos set and get *******************************
   
    public void setLegajo(String legajo){
        this.legajo=legajo;    }
    public String getLegajo(){
        return legajo;    }
//******************** Metodos toString *******************************

    @Override
    // heredo el tostring de persona 
    public String toString(){
        return super.toString()+" legajo:"+legajo;
    }
//******************** Metodos Heredados *******************************
 
    @Override
    public void obtener(){
        // invoco al metodo obtener de mi Padre
        super.obtener();// aca ingresa el nombre y edad por teclado
        System.out.println("Ingrese el legajo:");
        Scanner teclado= new Scanner(System.in);
        setLegajo(teclado.nextLine());
    }
    // Debo sobre-escribir el metodo abstracto
    public void presentarse(){
        System.out.println("Soy el docente(nombre) "+getNombre()); // fue heredada
        System.out.println("Soy el docente(edad) "+getEdad()); // fue heredada
        System.out.println("Soy el docente(sexo) "+getSexo()); // fue heredada
         System.out.println("Soy el docente(sexo).Static "+B0_Persona.getSexo()); // no uso herencia, sino q accedo con una direccion
        
        System.out.println("Mi legajo es:"+getLegajo()); // pertenece a la class
    }
//******************** Metodos implementado *******************************

    // Debo implementar el metodo cantar
    public void cantar(){
        System.out.println("Soy un docente y canto lo, lo, lo,.....");
    }
    public void vosFina(String cc){
        cc="finaaaaaaaa.....";
        System.out.println("Soy un docente con la voz fina "+cc);
    }
    public String vosAguda(){
        String cantare="Soy un docente con la voz Aguda GRUESAAAAAAAAAAA,.....";
        return cantare;
    }
}
