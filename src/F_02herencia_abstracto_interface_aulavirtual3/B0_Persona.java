package F_02herencia_abstracto_interface_aulavirtual3;

import java.util.Scanner;

public abstract class B0_Persona implements C1_Cantante{ //se usa interface(implements)
//********************* atributo ************************************
    private String nombre;
    private int edad;
    private A0_Domicilio direccion=new A0_Domicilio();
// Atributo de clase
    private static String sexo;
    
//********************* constructor ************************************      
    public B0_Persona(){  } // Constructor vacio
    // Constructor con 2 parametros
    public B0_Persona(String nombre,int edad){
        this.nombre=nombre;
        this.edad=edad;
    }
    // cONSTRUCTOR CON 2 PARAMETROS
    public B0_Persona(int edad, String nombre){
        this.nombre=nombre;
        this.edad=edad;
    }
    // Constructor con domicilio
    public B0_Persona(String nombre,int edad, A0_Domicilio direccion){
        this.nombre=nombre;
        this.edad=edad;
        this.direccion=direccion;
    }
//********************* get and set ************************************      
   
    public void setNombre(String nombre){
        this.nombre=nombre;    }
    public String getNombre(){
        return this.nombre;    }
    public void setEdad(int edad){
        this.edad=edad;    }
    public int getEdad(){
        return this.edad;    }
    
    // Metodos del atributo de la clase static
    static public String getSexo(){
        return sexo;    }    
    static public void setSexo(String sexo){
        B0_Persona.sexo=sexo;    } 
 //****************************** metodos de la clase **************************************
   
    public void obtener(){
        System.out.println("Ingrese su nombre:");
        Scanner teclado = new Scanner(System.in);
        nombre = teclado.nextLine();
        do {
            System.out.println("Ingresar edad:");
            edad =teclado.nextInt();
        } while (edad<=0);
        // Agrego este metodo, cuando agrego como atributo domicilio instanciado
        direccion.obtener();
    }
    public boolean esMayor(){
        boolean es=false;
        if (edad>18){
            es=true;
        }
        return es;
    }
 //****************************** toString **************************************
   
    @Override
    public String toString(){
    // Muestre el estado del objeto
    return "Nombre:"+this.nombre + " Edad:"+this.edad+direccion.toString();
            }
 //****************************** metodos abstracto **************************************
   
    // Genero un metodo abstracto
        // public abstract void metodoAbstracto();    
    // Declaro el metodo presentarse, como es absatracto no necesita tener las llaves 
    public abstract void presentarse();
    
    // agrego el metodo cantar cuando implemento la interface C1_Cantante
    @Override
    public abstract void cantar();
    
    
/*      
    HERENCIA:  
            i) para q se de la herencia, una clase tiene q heredar atributos y metodos del otro.
            ii) la clase padre es la q va heredar a la clase hija             
                    en la clase hija para q herede  se coloca: 
                EJ: public class ´hija´ EXTENDS ´padre´{}.
                    En el met. costructor se lo coloca SUPER(´nombre de los atributos´), 
                    en el met. toString se coloca tambien SUPER tostring( lo q ara es tomar el ToSristrin de la clase padre e 
                    implementarlo a la clase hija ), ojo q tiene q tener la misma letra tanto mayuscula y minuscula.                           
            iii) los atributos y metodos dela clase (Static), no se pueden instanciar.
    toStrong-(GET,SET):
            .) una ves instaciado la clase en un metodo, se puede acceder a los metodos y atributos, pero
            si los atributos son del tipo PRIVATE, se puede aceder atraves del metodo get, 
            e insertar atraves del metodo set.
            :) el ´toString´ es una metodo para devolver los valores de los atributos una ves insertado x el SET.
            :.) el ´@Override´ lo q haces sobre escribir el metodo, y se da mayormente en el( toString, metodos creados etc..)
    ABSTRACTO: 
            i) se utiliza como super clase.
            ii) Nose puede instanciar objetos.
            iii) SIrve para proporcionar una super clase apropiada a partir de la cual heredan las clases.
            iV) si un metodo es abstracto entonces x ende la clase tambien sera abstracta.
                EJ:   public  abstracto class Padre{
                           public abstracto void metodo(); }
            v) las clases abstractas no se pueden instaciar(osea no crear obj. de esa clase), solo se pueden heredar.       
                los metodos abtractos termina con ´;´. 
                EJ:  public abstract void metodo();
    INTERFACES: 
            i) todo los metodos son abtractos de la clase cantante, no va ser necesario poner la palabra reservada ABSTRACT.
                EJ: la clase sera sustituido x interface ==> ´public interface C1_Cantante{}´
            ii) las interfaces se implementan uno o mas, no se heredan.   
                EJ: en ves de un extends va mos usar ´implements´ ==> ´public class Canario implements Cantante1,Cantante2{ }´  
            iii) se sobre escribe(@Override) todos los metodos abstractos perteneciantes al interface de la clase cantante.
            iv) Una interface interface es una colección de operaciones es una colección de operaciones
                que especifican un servicio de una clase o que especifican un servicio de una clase o
                componente, es decir, un comportamiento componente, es decir, un comportamiento
                externamente visible de ese elemento. externamente visible de ese elemento.
                • Se especifican las operaciones externamente Se especifican las operaciones externamente
                  visibles sin especificación de la estructura interna. visibles sin especificación de la estructura interna.
    ---------------------------  link   -----------------------------------------------------------------------------------------------
        https://es.quora.com/Cu%C3%A1l-es-la-principal-diferencia-entre-herencia-e-interfaz-en-Java   
    */
    
}
