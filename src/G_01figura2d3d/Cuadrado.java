package G_01figura2d3d;

public class Cuadrado extends Figura2D{
    private int lado;
    
    public Cuadrado(){
        
    }
    public Cuadrado(String nombre,int grosorBorde,int color,int lado){
        super(nombre,grosorBorde,color);
        this.lado=lado;
    }
    public int getLado(){
        return lado;
    }
    public void setLado(int lado){
        this.lado=lado;
    }
    // Implemento 4 metodos
    @Override
    public int calcularArea(){
        return (lado*lado);
    }
    @Override
    public int calcularPerimetro(){
        return (4*lado);
    }
    @Override
    public void dibujar(){
        System.out.println("Soy un cuadrado");
    }
    // Cada vez que llamo incremento el lado un 10%
    public int cambiarTamanio(){
        lado*=1.1;
        return 10;
    }
 }
