package G_01figura2d3d;

public class TestFigura {
    public static void main(String[] args) {
        Cuadrado cuad1=new Cuadrado("cuadrado1",1,99,10);
        System.out.println("Area:"+cuad1.calcularArea());
        System.out.println("Perimetro:"+cuad1.calcularPerimetro());
        cuad1.dibujar();
        Cubo cubo1=new Cubo("cubo1",1,99,cuad1);
        cubo1.dibujar();
        System.out.println("Volumen:"+cubo1.calcularVolumen());
        System.out.println("-----------------------------------");
        System.out.println("Cambio tamaño:"+cuad1.cambiarTamanio());
        System.out.println("Area:"+cuad1.calcularArea());
        System.out.println("Perimetro:"+cuad1.calcularPerimetro());
        cubo1.dibujar();
        System.out.println("Volumen:"+cubo1.calcularVolumen());
        System.out.println("******aplico polimorfismo*******");
        Figura fig; // variable polimorfica
        fig = new Cuadrado();
        fig.dibujar();
        fig=new Cubo();
        fig.dibujar();
        
        
        
        
        
        
        
    }
}
