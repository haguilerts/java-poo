package G_01figura2d3d;

public abstract class Figura {
    protected String nombre;
    protected int grosorBorde;
    protected int color;
    
    public Figura(){
        
    }
    public Figura(String nombre, int grosorBorde,int color){
        this.nombre=nombre;
        this.grosorBorde=grosorBorde;
        this.color=color;
    }
    
    public String getNombre(){
        return nombre;
    }
    public int getGrosorBorde(){
        return grosorBorde;
    }
    public int getColor(){
        return color;
    }
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public void setGrosorBorde(int grosorBorde){
        this.grosorBorde=grosorBorde;
    }
    public void setColor(int color){
        this.color=color;
    }
    // metodo abstracto va depender de cada figura
    public abstract void dibujar();

}
