package G_01figura2d3d;

public class Cubo extends Figura3D{
    private Cuadrado cuadrado;
    
    public Cubo(){
        
    }
    public Cubo(String nombre, int grosorBorde,int color,Cuadrado cuadrado){
        super(nombre,grosorBorde, color);
        this.cuadrado=cuadrado;
    }
    public Cuadrado getCuadrado(){
        return cuadrado;
    }
    public void setCuadrado(Cuadrado cuadrado){
        this.cuadrado=cuadrado;
    }
    @Override
    public void dibujar(){
        System.out.println("Soy un cubo");
    }
    @Override
    public int calcularVolumen(){
        return (int) Math.pow(cuadrado.getLado(),3);
    }
}
