package E_01aulavirtual2;

import java.util.Scanner;

public class Domicilio {

    // Atributos
    private String calle;//nombre de la calle
    private int piso;   // nro del domicilio
    private String departamento;    // 8 C puede estar vacio
    private String localidad;   // Ciudadela
    private String provincia;   // Cordoba
// Metodos constructor
    //vacio

    public Domicilio() {
    }

    // con 5 argumentos
    public Domicilio(String calle, int piso, String departamento,
            String localidad, String provincia) {
        this.calle = calle;
        this.piso = piso;
        this.departamento = departamento;
        this.localidad = localidad;
        this.provincia = provincia;
    }

    // Metodo mostrar
    public void mostrar() {
        System.out.println("Calle:" + calle);
        System.out.println("Numero:" + piso);
        System.out.println("Departamento:" + departamento);
        System.out.println("Localidad:" + localidad);
        System.out.println("Provincia:" + provincia);
    }

    // Metodo obtener
    // Permite el ingreso del domicilio
    public void obtener() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Ingrese la calle:");
        calle = teclado.nextLine();
        System.out.println("Ingrese el numero:");
        piso = teclado.nextInt();
        teclado.nextLine();// limpio el buffer
        System.out.println("Ingrese el departamento:");
        departamento = teclado.nextLine();
        System.out.println("Ingrese la localidad:");
        localidad = teclado.nextLine();
        System.out.println("Ingrese la provincia");
        provincia = teclado.nextLine();
    }
    public String toString(){
        return "Calle:"+calle+" numero:"+piso+" departamento"+
                departamento+" localidad:"+localidad+" provincia"+
                provincia;
    }
}
