package E_01aulavirtual2;

import java.util.Scanner;

public class Docente extends Persona{
    // Atributos
    private String legajo;
    // Metodos constructores
    public Docente(){
        
    }
        // Metodo constructor con 3 parametros
    public Docente(String nombre,int edad,String legajo){
        // super(nombre,edad);  // llamo al 2do constructor de Persona
        super(edad,nombre);     // llamo al ul 3er constructor de Persona
        this.legajo=legajo;
    }
    
    // Metodo constructor con 4 parametros
    public Docente(String nombre,int edad,String legajo, Domicilio domicilio){
        super(nombre,edad,domicilio);
        this.legajo=legajo;
    }
    
    public void setLegajo(String legajo){
        this.legajo=legajo;
    }
    public String getLegajo(){
        return legajo;
    }
    // toString
    @Override
    public String toString(){
        return super.toString()+" legajo:"+legajo;
    }
    // Metodo Obtener
    @Override
    public void obtener(){
        // invoco al metodo obtener de mi Padre
        super.obtener();
        System.out.println("Ingrese el legajo:");
        Scanner teclado= new Scanner(System.in);
        setLegajo(teclado.nextLine());
    }
    // Debo sobre-escribir el metodo abstracto
    @Override
    public void presentarse(){
        System.out.println("Soy el docente "+getNombre());
        System.out.println("Mi legajo es:"+getLegajo());
    }
}
