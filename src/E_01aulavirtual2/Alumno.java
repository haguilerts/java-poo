package E_01aulavirtual2;

import java.util.Scanner;

public class Alumno extends Persona{
    // Atributos de la clase Alumno
    private String matricula;
    
   // Metodo constructor vacio
    public Alumno(){
    }
    // Metodo constructor con 3 parametros
    public Alumno(String nombre,int edad,String matricula){
        // super(nombre,edad);  // llamo al 2do constructor de Persona
        super(edad,nombre);     // llamo al ul 3er constructor de Persona
        this.matricula=matricula;
    }
    
    // Metodo constructor con 4 parametros
    public Alumno(String nombre,int edad,String matricula, Domicilio domicilio){
        super(nombre,edad,domicilio);
        this.matricula=matricula;
    }
    
    // Metodos getter y setter de Alumnos
    public void setMatricula(String matricula){
        this.matricula=matricula;
    }
    public String getMatricula(){
        return this.matricula;
    }
    // Metodo toString lo sobre-escribo
    public String toString(){
        return super.toString()+ " Matricula:"+this.matricula;
    }
    
    
    // Metodo obtener
    @Override
    public void obtener(){
        // Accedo al metodo obtener de la superclase
        // con el operador super
        super.obtener();
        System.out.println("Ingrese la matricula del alumno:");
        Scanner teclado=new Scanner(System.in);
        matricula=teclado.nextLine();
    }    
    // Genero un metodo abstracto
    // Implemento el metodo abstracto heredado
    // De la clase Persona
    public void metodoAbstracto(){
        
    }
    
    // Implemento el metodo presentarse de la clase Persona
    // Sobreescribo el metodo
    @Override
    public void presentarse(){
        System.out.println("Soy el alumno:"+getNombre());
        System.out.println("Mi matricula es:"+getMatricula());
    }
    
    
}
