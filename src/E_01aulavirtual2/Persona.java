package E_01aulavirtual2;

import java.util.Scanner;

public abstract class Persona {
    
// Atributos
    private String nombre;
    private int edad;
    private Domicilio direccion=new Domicilio();

// Atributo de clase
    private static String sexo;
    
    // Constructor vacio
    public Persona(){
        
    }
    // Constructor con 2 parametros
    public Persona(String nombre,int edad){
        this.nombre=nombre;
        this.edad=edad;
    }
    // cONSTRUCTOR CON 2 PARAMETROS
    public Persona(int edad, String nombre){
        this.nombre=nombre;
        this.edad=edad;
    }
    // Constructor con domicilio
    public Persona(String nombre,int edad, Domicilio direccion){
        this.nombre=nombre;
        this.edad=edad;
        this.direccion=direccion;
    }
    
    // Setters y getters
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return this.nombre;
    }
    public void setEdad(int edad){
        this.edad=edad;
    }
    public int getEdad(){
        return this.edad;
    }
    // Metodos del atributo de la clase
    static public String getSexo(){
        return sexo;
    }
    
    static public void setSexo(String sexo){
        Persona.sexo=sexo;
    }
    
    // toString
    @Override
    public String toString(){
    // Muestre el estado del objeto
    return "Nombre:"+this.nombre + " Edad:"+this.edad+direccion.toString();
            }
    
    // Metodos de la clase
    public void obtener(){
        System.out.println("Ingrese su nombre:");
        Scanner teclado = new Scanner(System.in);
        nombre = teclado.nextLine();
        do {
            System.out.println("Ingresar edad:");
            edad =teclado.nextInt();
        } while (edad<=0);
        // Agrego este metodo cuando agrego como atributo domicilio
        direccion.obtener();
    }
    public boolean esMayor(){
        boolean es=false;
        if (edad>18){
            es=true;
        }
        return es;
    }
    
    
    
    // Genero un metodo abstracto
//    public abstract void metodoAbstracto();
        
    // Declaro el metodo presentarse
    public abstract void presentarse();
    
}
