package B_02aritmetica2;
    public class Aritmetica {
        //////// atributos de la clase ////////
                int num1;
                int num2;

        //////// Metodos Constructores ////////

                //Metodo constructor vacio
               public Aritmetica(){

                }
                //Metodo constructor con 2 argumentos
                Aritmetica(int a,int b){
                    num1=a;
                    num2=b;
                }

        //////// Declarar los metodos de la clase ////////
                int sumar(){
                    return num1+num2;
                }
                int restar(){
                    return num1-num2;
                }
                int dividir(){
                    return num1/num2;
                }
                int multi(){
                    return num1*num2;
                }

    }
